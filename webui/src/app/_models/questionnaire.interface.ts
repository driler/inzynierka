export interface Questionnaire {
  name: string;
  creatorId: string;
  date: string;
  group: string;
  anonymous: boolean;
  oneTime: boolean;
  description: string;
  questions: Question[];
}

export interface Question {
  question: string;
  type: string;
  answers: Answer[];
}

export interface Answer {
  answer: string;
}
