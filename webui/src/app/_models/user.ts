export class User {
  firstname: string;
  groupSet: string[];
  id: number;
  email: string;
  accountActivated: boolean;
  lastname: string;
  username: string;
}
