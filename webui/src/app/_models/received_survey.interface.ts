export interface Survey {
  id: string;
  title: string;
  description: string;
  creator: string;
  creationDate: any;
  expirationDate: any;
  groups: any;
  questions: Questions[];
  open: boolean;
}

export interface Questions {
  answers: Answers[];
  id: number;
  question: string;
  type: string;
}

export interface Answers {
  content: string;
  id: number;
  answerType: string;
}
