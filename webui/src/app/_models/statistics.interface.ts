export interface StatisticsI {
  title: string;
  expirationDate: string;
  description: string;
  numberOfFilling: number;
  questionSummaryList: QuestionSummary[];
}

export interface QuestionSummary {
  question: string;
  answers: StatAnswer[];
  usersAnswers: UserAnswer[];
}

export interface StatAnswer {
  content: string;
  numberOfAnswers: number;
}

export interface UserAnswer {
  content: string;
  usersAnswers: string[];
}
