import {Component, Input, OnInit} from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';

import { Answer } from '../_models/questionnaire.interface';

@Component({
  selector: 'answer-array',
  templateUrl: './answer_array.component.html',
})

export class AnswerArrayComponent implements OnInit {
  @Input() parentForm: FormGroup;

  @Input() answers: Answer[];

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.parentForm.addControl('answers', new FormArray([]));
  }

  addAnswer(index: number) {
    this.answers.push({
      answer: ''
    });
  }

  removeAnswer(index: number) {
    this.answers.splice(index, 1);
    (<FormArray>this.parentForm.get('answers')).removeAt(index);
  }
}
