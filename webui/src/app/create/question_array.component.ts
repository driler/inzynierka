import {Component, Input, OnInit} from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';

import { Question } from '../_models/questionnaire.interface';

@Component({
  selector: 'question-array',
  templateUrl: './question_array.component.html',
})
export class QuestionArrayComponent implements OnInit {

  @Input() parentForm: FormGroup;

  @Input() questions: Question[];

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.parentForm.addControl('questions', new FormArray([]));
  }

  addAnswer(index: number) {
    this.questions.push({
      question: '',
      type: '',
      answers: []
    });
  }

  removeAnswer(index: number) {
    if (this.questions.length > 1) {
      this.questions.splice(index, 1);
      (<FormArray>this.parentForm.get('questions')).removeAt(index);
    }
  }
}
