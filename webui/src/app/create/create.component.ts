import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import {CreateQuestionnaireService} from "../_services/create_questionnaire.service";
import {Router, Routes} from "@angular/router";
import {Questionnaire} from "../_models/questionnaire.interface";
import {BlankSurveyService} from "./blank_survey.service";

@Component({
  moduleId: module.id,
  selector: 'create',
  templateUrl: 'create.component.html',
})
export class CreateComponent implements OnInit {
  error: any;

  groups: string[];

  data: Questionnaire;

  surveyForm: FormGroup

  constructor(private fb: FormBuilder,
              private create_q: CreateQuestionnaireService,
              private router: Router,
              private blank_survey: BlankSurveyService) {}

  ngOnInit() {
    this.data = this.blank_survey.getTrust();
    this.surveyForm = this.toFormGroup(this.data);
    this.groups = JSON.parse(localStorage.getItem('currentUser')).groupSet;
  }

  toFormGroup(data: Questionnaire) {
    return this.fb.group({
      name: data.name,
      creatorId: JSON.parse(localStorage.getItem('currentUser')).id,
      date: data.date,
      group: data.group,
      anonymous: data.anonymous,
      oneTime: data.oneTime,
      description: data.description,
    });
  }

  Submit() {
    this.create_q.create(this.surveyForm.value)
      .subscribe(
        (data) => {
          console.log(data);
          this.router.navigate(['']);

          // login successful
        },
        error => this.logError(error)
      );

  }

  logError(err: any) {
    this.error = err;
  }
}
