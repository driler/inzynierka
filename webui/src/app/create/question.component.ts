import {Component, Output, EventEmitter, Input, ChangeDetectorRef, OnInit} from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { Question } from '../_models/questionnaire.interface';
import {QuestionTypes} from "../_models/question.types";

const resolvedPromise = Promise.resolve(undefined);

@Component({
  selector: 'question',
  templateUrl: './question.component.html',
})
export class QuestionComponent implements OnInit {

  @Input() formArray: FormArray;

  @Input() question: Question;

  questionGroup: FormGroup;
  questionTypes = QuestionTypes;

  index: number;

  @Output() removed = new EventEmitter();

  constructor(private fb: FormBuilder, private cdRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.questionGroup = this.toFormGroup(this.question);

    resolvedPromise.then(() => {
      this.index = this.formArray.length;
      this.formArray.push(this.questionGroup);
    });
  }

  toFormGroup(question: Question) {
    return this.fb.group({
      name: question.question,
      type: question.type
    });
  }

  onChange(event: Event): void {
      //console.log("open");
  }
}
