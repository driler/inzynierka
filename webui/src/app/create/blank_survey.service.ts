import { Injectable } from '@angular/core';
import { Questionnaire } from '../_models/questionnaire.interface';

@Injectable()
export class BlankSurveyService {
  getTrust(): Questionnaire {
    return {
      name: '',
      creatorId: '',
      date: '',
      group: '',
      anonymous: false,
      oneTime: true,
      description: '',
      questions: [{
        question: '',
        type: '',
        answers: [{
          answer: ''
        }]
      }]
    };
  }
}
