import {Component, Output, EventEmitter, Input, ChangeDetectorRef, OnInit} from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import {Answer} from "../_models/questionnaire.interface";

const resolvedPromise = Promise.resolve(undefined);

@Component({
  selector: 'answer',
  templateUrl: './answer.component.html',
})
export class AnswerComponent implements OnInit {
  @Input() formArray: FormArray;

  @Input() answer: Answer;

  answerGroup: FormGroup;

  index: number;

  @Output() removed = new EventEmitter();

  constructor(private fb: FormBuilder, private cdRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.answerGroup = this.toFormGroup(this.answer);

    resolvedPromise.then(() => {
      this.index = this.formArray.length;
      this.formArray.push(this.answerGroup);
    });
  }

  toFormGroup(contract: Answer) {
    return this.fb.group({
      answer: ''
    });
  }
}
