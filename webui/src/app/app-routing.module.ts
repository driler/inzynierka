import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent }   from './login/login.component';
import { AppComponent }   from './app.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './_auth/auth';
import {CreateComponent} from "./create/create.component";
import {AuthSuperGuard} from "./_auth/authsuper";
import {SurveyComponent} from "./survey/survey.component";
import {SurveyResolve} from "./survey/survey.resolve";
import {GetSurveyService} from "./_services/get_survey.service";
import {UserPanelComponent} from "./panel/user_panel.component";
import {FilledSurveysComponent} from "./filled_surveys/filled_surveys.component";
import {CreatedComponent} from "./created/created.component";
import {StatisticsComponent} from "./created/statistics.component";
import {StatisticsResolve} from "./created/statistics.resolve";
import {FilledResolve} from "./filled_surveys/filled.resolve";
import {FilledComponent} from "./filled_surveys/filled.component";
import {Survey} from "./_models/received_survey.interface";
import {RegisterComponent} from "./register/register.component";

const routes: Routes = [
	{ path: '', component: HomeComponent, canActivate: [AuthSuperGuard] },
  { path: 'login',  component: LoginComponent },
  { path: 'register',  component: RegisterComponent },
  { path: 'create',  component: CreateComponent, canActivate: [AuthSuperGuard] },
  { path: 'panel',  component: UserPanelComponent, canActivate: [AuthGuard] },
  { path: 'filled',  component: FilledSurveysComponent, canActivate: [AuthSuperGuard] },
  { path: 'filled/:id/:filledId',  component: FilledComponent, canActivate: [AuthSuperGuard], resolve:
    {
      filled: FilledResolve,
      survey: SurveyResolve
    }
  },
  { path: 'created',  component: CreatedComponent, canActivate: [AuthSuperGuard] },
  { path: 'created/:id', component: StatisticsComponent, canActivate: [AuthSuperGuard], resolve: {
    stats: StatisticsResolve,
  }},
  { path: 'survey/:id',  component: SurveyComponent, canActivate: [AuthSuperGuard], resolve: {
      survey: SurveyResolve
    }
  },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '**', redirectTo: '/' }

];

@NgModule({
	imports: [ RouterModule.forRoot(routes) ],
	exports: [ RouterModule ],
  providers: [ GetSurveyService, SurveyResolve ]
})
export class AppRoutingModule {}
