import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../_models/user';

import { AuthService } from '../_services/auth.service';

@Component({
  moduleId: module.id,
  templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
  model: any = {};
  error = '';

  constructor(
    private router: Router,
    private authenticationService: AuthService) { }

  ngOnInit(): void {
    // reset login status
    this.authenticationService.logout();
  }

  logIn() {
    this.authenticationService.login(this.model.username, this.model.password)
      .subscribe(
        (data) => {
          console.log();
          let user: User = data.userData;
          let token = data.token;
          if (token) {
                  // store username and jwt token in local storage to keep user logged in between page refreshes
                  localStorage.setItem('currentUser', JSON.stringify(user));
                  this.router.navigate(['/']);

                }
          // login successful
        },
        (err) => {
          this.error = 'Username or password is incorrect';
          this.model.password = '';
        }
    );
  }
}
