import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import {MatRadioModule} from '@angular/material/radio';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { AlertModule } from 'ngx-bootstrap';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { NavComponent } from './navigation/nav.component';
import { CreateComponent } from './create/create.component';

import { AuthService } from './_services/auth.service';

import { AuthGuard } from './_auth/auth';
import { AuthSuperGuard } from './_auth/authsuper';
import {QuestionComponent} from "./create/question.component";
import {AnswerComponent} from "./create/answer.component";
import {CreateQuestionnaireService} from "./_services/create_questionnaire.service";
import {SurveyComponent} from "./survey/survey.component";
import {GetSurveyService} from "./_services/get_survey.service";
import {UserPanelComponent} from "./panel/user_panel.component";
import {UserService} from "./_services/user.service";
import {SurveyMultiQuestionComponent} from "./survey/survey_multi_question.component";
import {SurveySingleQuestionComponent} from "./survey/survey_single_question.component";
import {FillSurveyService} from "./_services/send_filled_survey.service";
import {QuestionArrayComponent} from "./create/question_array.component";
import {AnswerArrayComponent} from "./create/answer_array.component";
import {BlankSurveyService} from "./create/blank_survey.service";
import {FilledSurveysComponent} from "./filled_surveys/filled_surveys.component";
import {CreatedComponent} from "./created/created.component";
import {StatisticsComponent} from "./created/statistics.component";
import {StatisticsResolve} from "./created/statistics.resolve";
import {FilledResolve} from "./filled_surveys/filled.resolve";
import {FilledComponent} from "./filled_surveys/filled.component";
import {ChartsModule} from "ng2-charts";
import {NgxChartsModule} from "@swimlane/ngx-charts";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {StatisticsQuestionComponent} from "./created/statistics_question.component";
import {RegisterComponent} from "./register/register.component";

@NgModule({
  imports:      [
    AlertModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatRadioModule,
    MatCheckboxModule,
    NgxChartsModule,
    BrowserAnimationsModule
  ],
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    HomeComponent,
    NavComponent,
    CreateComponent,
    QuestionArrayComponent,
    QuestionComponent,
    AnswerArrayComponent,
    AnswerComponent,
    UserPanelComponent,
    SurveyComponent,
    SurveyMultiQuestionComponent,
    SurveySingleQuestionComponent,
    FilledSurveysComponent,
    CreatedComponent,
    StatisticsComponent,
    FilledComponent,
    StatisticsQuestionComponent
  ],
  providers: [
    AuthGuard,
    AuthService,
    AuthSuperGuard,
    GetSurveyService,
    CreateQuestionnaireService,
    UserService,
    FillSurveyService,
    BlankSurveyService,
    StatisticsResolve,
    FilledResolve
  ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
