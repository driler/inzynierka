import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "../_services/auth.service";

@Component({
  moduleId: module.id,
  selector: 'nav',
  templateUrl: 'nav.component.html'
})

export class NavComponent implements OnInit {
  superRole = false;

  constructor(
    private router: Router,
    private authenticationService: AuthService
  ) { }

  ngOnInit(): void {
    if (localStorage.getItem('currentUser') !== null) {
      this.superRole = JSON.parse(localStorage.getItem('currentUser')).superRole;
    } else {
      this.superRole = false;
    }
  }

  navigate(path: String): void {
    this.router.navigate(['/' + path]);
  }


  logOut(): void {
    this.authenticationService.logout();
  }
}
