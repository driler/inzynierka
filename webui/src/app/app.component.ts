import {Component, OnInit} from '@angular/core';
import {AuthService} from "./_services/auth.service";

@Component({
  selector: 'my-app',
  template: `
  <h1>Ankieter</h1>
  <router-outlet (activate)="changeOfRoutes()"></router-outlet>
  `,
})
export class AppComponent {

  constructor(private auth: AuthService) {}

  changeOfRoutes() {
    this.auth.refresh();
  }
}
