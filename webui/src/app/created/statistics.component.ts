import {NavComponent} from "../navigation/nav.component";
import {Component, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {StatisticsI} from "../_models/statistics.interface";

@Component({
  moduleId: module.id,
  selector: 'statistics',
  templateUrl: './statistics.component.html'
})

export class StatisticsComponent implements OnInit {
  survey: StatisticsI;
  times: string;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.survey = this.route.snapshot.data['stats'];
    if (this.survey.numberOfFilling === 1) {
      this.times = 'time';
    } else {
      this.times = 'times';
    }
  }
}
