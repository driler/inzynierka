///<reference path="../navigation/nav.component.ts"/>
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../_models/user';
import { NavComponent } from '../navigation/nav.component';
import 'rxjs/add/operator/map';
import {GetSurveyService} from "../_services/get_survey.service";
import {Survey} from "../_models/received_survey.interface";

@Component({
  selector: 'created',
  templateUrl: './created.component.html',
  entryComponents: [ NavComponent ]
})

export class CreatedComponent implements OnInit {
  user: User;
  surveyList: Survey[] = [];

  constructor(private router: Router, private getSurvey: GetSurveyService) {
  }


  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.getSurvey.getCreatedSurveys(this.user.id.toString())
      .subscribe(
        (data) => {
          this.surveyList = <any>data;
        }
      );
  }

  getSurveyStatistics(id: number) {
    //this.getSurvey.getSurveyStatistics(id.toString(), this.user.id.toString())
    //  .subscribe((data) => console.log(data));
    this.router.navigate(['created/' + id]);
  }
}
