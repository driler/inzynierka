import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import {GetSurveyService} from "../_services/get_survey.service";
import {Survey} from "../_models/received_survey.interface";

@Injectable()
export class StatisticsResolve implements Resolve<any> {

  constructor(private getSurveyService: GetSurveyService) {}

  resolve(route: ActivatedRouteSnapshot) {
    const userId = JSON.parse(localStorage.getItem('currentUser')).id;
    return this.getSurveyService.getSurveyStatistics(route.params['id'], userId);
  }
}
