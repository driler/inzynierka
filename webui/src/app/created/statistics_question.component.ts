import {Component, Input, OnInit} from "@angular/core";
import {StatAnswer} from "../_models/statistics.interface";

@Component ({
  moduleId: module.id,
  selector: 'statistics-question',
  templateUrl: './statistics_question.component.html',
  styleUrls: ["./statistics_question.component.css"]

})


export class StatisticsQuestionComponent implements OnInit {
  @Input()
  answers: any[];
  @Input()
  userAnswers: any[];

  public height = 0;

  answersList: AnswerStat[] = [];

// options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showXAxisLabel = true;
  showYAxisLabel = true;

  colorScheme = {
    domain: ['green']
  };


  constructor() {
  }

  ngOnInit() {
    //console.log(this.answers);
    for (let answer of this.answers) {
      this.answersList.push({
        'name': answer.content,
        'value': answer.numberOfAnswers
      });
      this.height = this.height + 80;
    }
  }

  axisFormat(val: any) {
    return (val % 1 === 0 ) ? val.toLocaleString() : '' ;
  }
}

export interface AnswerStat {
  name: string;
  value: number;
}
