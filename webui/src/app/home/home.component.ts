///<reference path="../navigation/nav.component.ts"/>
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../_models/user';
import { NavComponent } from '../navigation/nav.component';
import 'rxjs/add/operator/map';
import {GetSurveyService} from "../_services/get_survey.service";
import {Survey} from "../_models/received_survey.interface";

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  entryComponents: [ NavComponent ]
})

export class HomeComponent implements OnInit {
  user: User;
  surveyList: Survey[] = [];

  constructor(private router: Router, private getSurvey: GetSurveyService) {
  }


  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.getSurvey.getSurveysForUser(this.user.username)
      .subscribe(
        (data) => {
          this.surveyList = <any>data;
        },
        (err) => {
          console.log(err);
        }
      );
  }

  fillSurvey(id: number) {
    this.router.navigate(['survey/' + id]);
  }
}
