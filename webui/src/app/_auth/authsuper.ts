import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import {AuthService} from "../_services/auth.service";
import {Http} from "@angular/http";

@Injectable()
export class AuthSuperGuard implements CanActivate {
  private http: Http;

  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (localStorage.getItem('currentUser') && JSON.parse(localStorage.getItem('currentUser')).accountActivated) {

      // logged in so return true
      return true;
    }

    // not logged in so redirect to login page with the return url
    this.router.navigate(['/panel']);
    return false;
  }
}
