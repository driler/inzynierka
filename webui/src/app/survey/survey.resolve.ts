import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import {GetSurveyService} from "../_services/get_survey.service";

@Injectable()
export class SurveyResolve implements Resolve<any> {

  constructor(private getSurveyService: GetSurveyService) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.getSurveyService.getSurvey(route.params['id']);
  }
}
