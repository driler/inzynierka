import {Component, NgModule, OnInit, Input} from '@angular/core'
import {BrowserModule} from '@angular/platform-browser'
import { FormControl, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {Answers, Questions} from "../_models/received_survey.interface";

@Component({
  selector: 'survey-multi-question',
  templateUrl: 'survey_multi_question.component.html',
})
export class SurveyMultiQuestionComponent implements OnInit {
  @Input('group') myForm: FormGroup;

  @Input('answers')
  answers: Questions;

  answerList = [];

  constructor() { }

  ngOnInit() {
    this.answerList = this.answers.answers;
  }

  onChange(answer: string, isChecked: boolean) {
    const answersFormArray = <FormArray>this.myForm.controls.answers;
    if(isChecked) {
      answersFormArray.push(new FormControl(answer));
    } else {
      let index = answersFormArray.controls.findIndex(x => x.value == answer)
      answersFormArray.removeAt(index);
    }
  }

}
