import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {Survey} from "../_models/received_survey.interface";

import {GetSurveyService} from "../_services/get_survey.service";
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {FillSurveyService} from "../_services/send_filled_survey.service";

@Component({
  moduleId: module.id,
  selector: 'survey',
  templateUrl: 'survey.component.html',
})

export class SurveyComponent implements OnInit {
  data: Survey;
  surveyForm: FormGroup;

  get questions() { return <FormArray>this.surveyForm.get('questions'); }

  constructor (private route: ActivatedRoute,
               private fb: FormBuilder,
               private fill_s: FillSurveyService,
               private router: Router) {

  }

  ngOnInit() {
    this.data = this.route.snapshot.data['survey'];

    this.surveyForm = this.fb.group({
      userId: [ JSON.parse(localStorage.getItem('currentUser')).id],
      surveyId: [this.data.id],
      questions: this.fb.array([])
    });

    for(let i of this.data.questions){
        this.addQuestion(i.id);
    }

    console.log(this.surveyForm.controls.questions);
  }

  addQuestion(id: number) {
    this.questions.push(this.fb.group({
      question: id,
      answers: this.fb.array([], Validators.required)
    }));
  }

  Submit() {
    this.fill_s.fill(this.surveyForm.value)
      .subscribe(
        () => {
          this.router.navigate(['']);

        },
        error => this.logError(error)
  );

  }

  logError(error: any) {
    console.log(error);
  }
}
