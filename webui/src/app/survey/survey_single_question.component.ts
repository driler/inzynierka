import {Component, NgModule, OnInit, Input} from '@angular/core'
import {BrowserModule} from '@angular/platform-browser'
import { FormControl, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {Answers, Questions} from "../_models/received_survey.interface";
import {MatRadioChange} from "@angular/material";

@Component({
  selector: 'survey-single-question',
  templateUrl: 'survey_single_question.component.html',
})
export class SurveySingleQuestionComponent implements OnInit {
  @Input('group') myForm: FormGroup;

  @Input('answers')
  answers: Questions;

  answerList = [];

  constructor() { }

  ngOnInit() {
    this.answerList = this.answers.answers;
  }

  onChange(answer: MatRadioChange) {
    const answersFormArray = <FormArray>this.myForm.controls.answers;
    answersFormArray.removeAt(0);
    answersFormArray.push(new FormControl(answer.value));

  }
}
