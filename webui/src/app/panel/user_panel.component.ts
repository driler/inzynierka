import {Component, OnInit} from "@angular/core";
import {NavComponent} from "../navigation/nav.component";
import {User} from "../_models/user";
import {AbstractControl, Form, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../_services/user.service";

function passwordMatcher(c: AbstractControl) {
  return c.get('newPassword').value === c.get('newPasswordTwo').value
    ? null : {nomatch: true};
}

@Component({
  selector: 'user-panel',
  templateUrl: './user_panel.component.html',
  entryComponents: [ NavComponent ]
})

export class UserPanelComponent implements OnInit {
  userData: User;
  emailChange: boolean;
  passwordChange: boolean;
  codeForm: FormGroup;
  newEmailForm: FormGroup;
  newPasswordForm: FormGroup;
  differentPasswords: boolean;
  wrongCode: boolean;

  constructor(private fb: FormBuilder, private userService: UserService) {}

  ngOnInit(): void {
    this.userData = JSON.parse(localStorage.getItem('currentUser'));
    this.emailChange = false;
    this.passwordChange = false;
    this.differentPasswords = false;
    this.codeForm = this.fb.group({
      code: ['', [Validators.required, Validators.minLength(10)]],
      username: [this.userData.username]
    });

    this.newEmailForm = this.fb.group({
      newEmail: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
      id: [this.userData.id],
      username: [this.userData.username]

    });
    this.newPasswordForm = this.fb.group({
      oldPassword: ['', [Validators.required]],
      newPassword: ['', [Validators.required, Validators.minLength(8)]],
      newPasswordTwo: ['', [Validators.required, Validators.minLength(8)]],

      id: [this.userData.id],
      username: [this.userData.username]

    }, {validator: passwordMatcher});

    this.wrongCode = false;
  }


  change(toChange: string) {
    if (toChange === 'email') {
      this.emailChange = !this.emailChange;
      this.passwordChange = false;
    }
    if (toChange === 'password') {
      this.emailChange = false;
      this.passwordChange = !this.passwordChange;
      this.differentPasswords = false;
    }
  }

  changeEmail() {
    this.userService.changeEmail(JSON.stringify(this.newEmailForm.value));
    console.log(this.newEmailForm.value);
  }

  changePassword() {
    if(this.newPasswordForm.value.newPassword === this.newPasswordForm.value.newPasswordTwo) {
      console.log(JSON.stringify(this.newPasswordForm.value));
      this.userService.changePassword(JSON.stringify(this.newPasswordForm.value));
    } else {
      this.differentPasswords = true;
    }
  }

  activateUser() {
    this.userService.activateUser(this.codeForm.value)
      .subscribe((data) => {
          console.log(data);
        },
        (err) => {
          if (err.status === 500) {
            this.wrongCode = true;
          }
        });
  }
}

