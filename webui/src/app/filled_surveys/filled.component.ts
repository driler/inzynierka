import {Component, OnInit} from "@angular/core";
import {NavComponent} from "../navigation/nav.component";
import {ActivatedRoute} from "@angular/router";
import {Survey} from "../_models/received_survey.interface";
import {FilledQuestion} from "../_models/filled_answers.interface";

@Component({
  selector: 'filled',
  templateUrl: './filled.component.html',
  entryComponents: [ NavComponent ]
})

export class FilledComponent implements OnInit {
  survey: Survey;
  answers: FilledQuestion[];

  constructor(private route: ActivatedRoute) {};

  ngOnInit(): void {
    this.survey = this.route.snapshot.data['survey'];
    this.answers = this.route.snapshot.data['filled'];
  }

  checked(qId: number, aId: number): boolean {
    for(let question of this.answers){
      if(question.question === qId) {
        for(let answer of question.answers){
          if(answer === aId) return true;
        }
      }
    }
    return false;
  }
}
