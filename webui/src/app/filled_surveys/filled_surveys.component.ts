import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { NavComponent } from '../navigation/nav.component';
import 'rxjs/add/operator/map';
import {User} from "../_models/user";
import {Survey} from "../_models/received_survey.interface";
import {GetSurveyService} from "../_services/get_survey.service";

@Component({
  selector: 'filled-surveys',
  templateUrl: './filled_surveys.component.html',
  entryComponents: [ NavComponent ]
})

export class FilledSurveysComponent implements OnInit {
  user: User;
  surveyList: Survey[] = [];

  constructor(private router: Router, private getSurvey: GetSurveyService) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.getSurvey.getFilledSurveys(this.user.username)
      .subscribe(
        (data) => {
          this.surveyList = (<any>data);
        },
        (err) => {
          console.log(err);
        }
      );

  }

  goToDetails(surveyId: string, filledId: string) {
    this.router.navigate(['filled/' + surveyId + '/' + filledId]);
}
}
