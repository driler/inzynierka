import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import {GetSurveyService} from "../_services/get_survey.service";

@Injectable()
export class FilledResolve implements Resolve<any> {

  constructor(private getSurveyService: GetSurveyService) {}

  resolve(route: ActivatedRouteSnapshot) {
    const userId = JSON.parse(localStorage.getItem('currentUser')).id;
    return this.getSurveyService.getFilledSurvey(route.params['id'], userId, route.params['filledId']);
  }
}
