import {Injectable} from "@angular/core";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/empty';
import {Observable} from "rxjs/Observable";
import {Router} from "@angular/router";


@Injectable()
export class GetSurveyService {

  constructor(private http: HttpClient, private router: Router) { }

  getSurvey(id: string) {
    return this.http.get('http://localhost:8081/surveys/survey?id=' + id)
      .retry(3)
      .catch((err: HttpErrorResponse) => {

        if (err.error instanceof Error) {
          console.error('An error occurred:', err.error.message);
        } else {
          console.error(`Backend returned code ${err.status}, body was: ${err.error}`);
        }
        this.router.navigate(['/']);

        return Observable.empty();
      });
  }

  getFilledSurveys(username: string) {
    return this.http.get('http://localhost:8081/surveys/getSurveyHistoryForUser?username=' + username)
      .retry(3)
      .catch((err: HttpErrorResponse) => {

        if (err.error instanceof Error) {
          console.error('An error occurred:', err.error.message);
        } else {
          console.error(`Backend returned code ${err.status}, body was: ${err.error}`);
        }
        this.router.navigate(['/filled']);

        return Observable.empty();
      });
  }

  getFilledSurvey(surveyId: string, userId: string, filledId: string) {
    return this.http.get('http://localhost:8081/surveys/getUserAnswersForSurvey?surveyId=' + surveyId
      + '&userId=' + userId
      + '&filledId=' + filledId)
      .retry(3)
      .catch((err: HttpErrorResponse) => {

        if (err.error instanceof Error) {
          console.error('An error occurred:', err.error.message);
        } else {
          console.error(`Backend returned code ${err.status}, body was: ${err.error}`);
        }
        this.router.navigate(['/']);

        return Observable.empty();
      });
  }

  getSurveysForUser(username: string) {
    return this.http.get('http://localhost:8081/surveys/getSurveysForUser?username=' + username)
      .retry(3)
      .catch((err: HttpErrorResponse) => {

        if (err.error instanceof Error) {
          console.error('An error occurred:', err.error.message);
        } else {
          console.error(`Backend returned code ${err.status}, body was: ${err.error}`);
        }
        this.router.navigate(['/']);

        return Observable.empty();
      });
  }

  getCreatedSurveys(id: string) {
    return this.http.get('http://localhost:8081/surveys/getSurveysCreatedByUser?id=' + id)
      .retry(3)
      .catch((err: HttpErrorResponse) => {

        if (err.error instanceof Error) {
          console.error('An error occurred:', err.error.message);
        } else {
          console.error(`Backend returned code ${err.status}, body was: ${err.error}`);
        }
        this.router.navigate(['/']);

        return Observable.empty();
      });
  }

  getSurveyStatistics(surveyId: string, userId: string) {
    return this.http.get('http://localhost:8081/surveys/getStatistics?userId=' + userId + '&surveyId=' + surveyId)
      .retry(3)
      .catch((err: HttpErrorResponse) => {

        if (err.error instanceof Error) {
          console.error('An error occurred:', err.error.message);
        } else {
          console.error(`Backend returned code ${err.status}, body was: ${err.error}`);
        }
        this.router.navigate(['/created']);

        return Observable.empty();
      });
  }
}
