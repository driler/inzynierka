import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";

@Injectable()
export class UserService {
  public token: string;

  constructor(private http: Http) {
    // set token if saved in local storage
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.token = currentUser && currentUser.token;
  }

  activateUser(body: any) {
    console.log(body);
    return this.http.post('http://localhost:8081/users/activateUser', body);
  }

  changePassword(passwordForm: string){
    let body = passwordForm;
    return this.http.post('http://localhost:8081/users/changePassword', body)
      .subscribe((data) => {
      console.log(data);
      });
  }

  changeEmail(emailForm: string){
    let body = emailForm;
    return this.http.post('http://localhost:8081/users/changeEmail', body)
      .map((response: Response) => response.json())
      .subscribe((data) => {
        console.log(data);
      });
  }
}
