import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import {Router} from "@angular/router";

@Injectable()
export class AuthService {
  public token: string;

  constructor(private http: Http, private router: Router) {
    // set token if saved in local storage
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.token = currentUser && currentUser.token;
  }

  register(data: any) {
    let body = data;
    return this.http.post('http://localhost:8081/auth/register', body)
  }

  login(username: string, password: string){
    let body = new URLSearchParams();
    body.set('username', username);
    body.set('password', password);
    return this.http.post('http://localhost:8081/auth/login', body)
      .map((response: Response) => response.json());
  }

  logout(): void {
    // clear token remove user from local storage to log user out
    this.http.get("http://localhost:8081/auth/logout")
      .map((response: Response) => response.json())
      .subscribe(
        (data) => {
          if (data.result === 'success') {
            this.token = null;
            localStorage.removeItem('currentUser');
            this.router.navigate(['/login']);
          }
        }
      );
  }

  refresh() {
    this.http.get("http://localhost:8081/auth/refresh")
      .map((response: Response) => response.json())
      .subscribe(
        (data) => {
          //console.log(data);
        }
      );
  }
}
