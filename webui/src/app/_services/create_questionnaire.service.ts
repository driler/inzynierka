import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";

@Injectable()
export class CreateQuestionnaireService {

  constructor(private http: Http) { }

  create(questionnaire: any) {
    let body = questionnaire;
    console.log(body);
    return this.http.post('http://localhost:8081/surveys/addSurvey', body)
      .map((response: Response) => response.json());
  }
}
