import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";

@Injectable()
export class FillSurveyService {

  constructor(private http: Http) { }

  fill(survey: any) {
    let body = survey;
    console.log(body);
    return this.http.post('http://localhost:8081/surveys/fillSurvey', body);
  }
}
