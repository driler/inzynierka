import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../_services/auth.service';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';

function passwordMatcher(c: AbstractControl) {
  return c.get('passwordOne').value === c.get('passwordTwo').value
    ? null : {nomatch: true};
}

@Component({
  moduleId: module.id,
  templateUrl: 'register.component.html'
})

export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  differentPasswords = false;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authenticationService: AuthService) { }

  ngOnInit(): void {
    this.registerForm = this.fb.group({
      username: ['', [Validators.required]],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      passwordOne: ['', [Validators.required, Validators.minLength(8)]],
      passwordTwo: ['', [Validators.required, Validators.minLength(8)]],
      email: ['', [Validators.required, Validators.email]]
    });
  }

  register(): void {
    if (this.registerForm.value.passwordOne === this.registerForm.value.passwordTwo) {
      this.authenticationService.register(this.registerForm.value)
        .subscribe(
          (data) => {
            console.log(data);
            this.router.navigate(['/login']);
          },
          (err) => {
            console.error(err);
          }
        );
    } else {
      this.differentPasswords = true;
    }
  }

  passwordFocus(): void {
    this.differentPasswords = false;
  }
}
