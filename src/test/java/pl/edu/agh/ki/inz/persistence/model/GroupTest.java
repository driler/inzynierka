package pl.edu.agh.ki.inz.persistence.model;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.HashSet;

import static org.junit.Assert.*;

/**
 * Created by Kisiel on 06.11.2017.
 */
public class GroupTest {
	@Test
	public void testSettersAndGetters(){
		Group group = new Group();
		group.setId(1L);
		group.setName("Some name");
		group.setGroupLeader(Mockito.mock(User.class));
		Assert.assertEquals(1L, (long)group.getId());
		Assert.assertEquals("Some name", group.getName());
		Assert.assertTrue(group.getSurveys().isEmpty());
		Assert.assertTrue(group.getUsers().isEmpty());
	}

	@Test
	public void addUserToGroup() throws Exception {
		Group group = new Group();
		group.addUserToGroup(Mockito.mock(User.class));
		Assert.assertEquals(1, group.getUsers().size());
	}

	@Test
	public void checkIfUserBelongsToGroup() throws Exception {
		User user = new User();
		User user2 = new User();
		User user3 = new User();
		user.setUsername("1");
		user2.setUsername("12");
		user3.setUsername("123");
		user.setEmail("1");
		user2.setEmail("12");
		user3.setEmail("123");

		Group group = new Group();
		group.addUserToGroup(user);
		group.addUserToGroup(user2);
		Assert.assertFalse(group.checkIfUserBelongsToGroup(user3));
		Assert.assertTrue(group.checkIfUserBelongsToGroup(user2));
		Assert.assertTrue(group.checkIfUserBelongsToGroup(user));
	}

}