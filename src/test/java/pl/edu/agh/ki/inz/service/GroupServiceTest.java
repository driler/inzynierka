package pl.edu.agh.ki.inz.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pl.edu.agh.ki.inz.persistence.model.Group;
import pl.edu.agh.ki.inz.persistence.model.User;
import pl.edu.agh.ki.inz.persistence.repository.GroupRepository;
import pl.edu.agh.ki.inz.persistence.repository.UserRepository;
import pl.edu.agh.ki.inz.service.interfaces.IGroupService;


import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;

/**
 * Created by Kisiel on 10.11.2017.
 */
public class GroupServiceTest {

	private GroupService groupService = new GroupService();
	private GroupRepository groupRepository = Mockito.spy(GroupRepository.class);
	private UserRepository userRepository = Mockito.spy(UserRepository.class);

	private Group group = new Group();
	private User user = new User();
	private User user2 = new User();
	private User user3 = new User();
	private User user4 = new User();

	@BeforeEach
	public void setup(){
		group.setName("name");
		user.setUsername("123213");
		user2.setUsername("12");
		user3.setUsername("123");
		user4.setUsername("1234");
		Mockito.when(userRepository.findByUsername("12")).thenReturn(Optional.of(user2));
		Mockito.when(userRepository.findByUsername("123")).thenReturn(Optional.of(user3));
		Mockito.when(groupRepository.findByName(anyString())).thenReturn(Optional.of(group));
		groupService.setGroupRepository(groupRepository);
		groupService.setUserRepository(userRepository);
	}

	@Test
	public void addUserToGroup() throws Exception {
		groupService.addUserToGroup(user, group);
		Assert.assertTrue(group.getUsers().size() == 1);
		groupService.addUserToGroup(user4,"name");
		Assert.assertTrue(group.getUsers().size() == 2);
		groupService.addUserToGroup("12", group);
		Assert.assertTrue(group.getUsers().size() == 3);
		groupService.addUserToGroup("123", "name");
		Assert.assertTrue(group.getUsers().size() == 4);
	}


	@Test
	public void returnUsersOfGroup() throws Exception {
	}

}