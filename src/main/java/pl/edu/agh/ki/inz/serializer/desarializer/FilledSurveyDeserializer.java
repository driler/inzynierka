package pl.edu.agh.ki.inz.serializer.desarializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import pl.edu.agh.ki.inz.util.data.FilledSurveyPrimaryData;

import java.io.IOException;

/**
 * Created by Kisiel on 30.11.2017.
 */
public class FilledSurveyDeserializer extends JsonDeserializer<FilledSurveyPrimaryData> {

	@Override
	public FilledSurveyPrimaryData deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		FilledSurveyPrimaryData surveyPrimaryData = new FilledSurveyPrimaryData();

		JsonNode node = p.getCodec().readTree(p);
		surveyPrimaryData.setSurveyId(node.get("surveyId").asText());
		surveyPrimaryData.setUserId(node.get("userId").asText());

		JsonNode questions = node.get("questions");
		questions.elements().forEachRemaining(q ->{
			FilledSurveyPrimaryData.SimpleQuestion question = new FilledSurveyPrimaryData.SimpleQuestion();
			question.setQuestion(q.get("question").asLong());
			q.get("answers").elements().forEachRemaining( a ->{
				question.addToList(a.asLong());
			});
			surveyPrimaryData.addToList(question);
		});

		return surveyPrimaryData;


	}
}
