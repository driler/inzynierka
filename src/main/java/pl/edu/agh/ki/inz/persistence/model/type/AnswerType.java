package pl.edu.agh.ki.inz.persistence.model.type;

/**
 * Created by Kisiel on 05.06.2017.
 */
public enum AnswerType {
    OPEN, CLOSE, IMAGE
}
