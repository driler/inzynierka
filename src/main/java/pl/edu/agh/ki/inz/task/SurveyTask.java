package pl.edu.agh.ki.inz.task;

import pl.edu.agh.ki.inz.persistence.model.Survey;
import pl.edu.agh.ki.inz.persistence.repository.SurveyRepository;

import java.util.TimerTask;

/**
 * Created by Kisiel on 25.11.2017.
*/
public abstract class SurveyTask extends TimerTask {
	protected Survey survey;
	SurveyRepository surveyRepository;

	SurveyTask() {
	}

	public void setSurvey(Survey survey) {
		this.survey = survey;
	}

	public void setSurveyRepository(SurveyRepository surveyRepository) {
		this.surveyRepository = surveyRepository;
	}
}
