package pl.edu.agh.ki.inz.util;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.Random;

/**
 * Created by Kisiel on 12.12.2017.
 */
public class Generator {

	public static String randomStringGenerator(int length){
		return randomStringGenerator(length, true, false).toUpperCase();
	}

	public static String randomStringGenerator(int length, boolean useLetters, boolean useNumbers){
		return RandomStringUtils.random(length, useLetters, useNumbers);
	}

	public static int randomInt(int max){
		return new Random().nextInt(max);
	}

	public static double randomDouble(){
		return Math.floor(Math.random() * 9_000_000_000L) + 1_000_000_000L;
	}
}
