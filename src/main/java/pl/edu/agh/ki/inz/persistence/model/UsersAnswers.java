package pl.edu.agh.ki.inz.persistence.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import pl.edu.agh.ki.inz.serializer.converter.QuestionConverter;

import javax.persistence.*;

/**
 * Created by Kisiel on 30.11.2017.
 */
//todo repair it
@Entity
public class UsersAnswers {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@ManyToOne
	@JoinColumn(name = "answer")
	private Answer answer;
	@ManyToOne
	@JsonSerialize(converter = QuestionConverter.class)
	@JoinColumn(name = "question")
	private Question question;

	@ManyToOne
	@JoinColumn(name = "fillingInfo")
	private SurveyUsersFilled fillingInfo;

	public UsersAnswers() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Answer getAnswer() {
		return answer;
	}

	public void setAnswer(Answer answer) {
		this.answer = answer;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public SurveyUsersFilled getFillingInfo() {
		return fillingInfo;
	}

	public void setFillingInfo(SurveyUsersFilled fillingInfo) {
		this.fillingInfo = fillingInfo;
	}
}
