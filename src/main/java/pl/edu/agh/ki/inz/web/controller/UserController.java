package pl.edu.agh.ki.inz.web.controller;


import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.edu.agh.ki.inz.persistence.model.User;
import pl.edu.agh.ki.inz.persistence.repository.UserRepository;
import pl.edu.agh.ki.inz.service.interfaces.IUserService;
import pl.edu.agh.ki.inz.web.exception.UserNotFoundException;

import javax.print.attribute.standard.Media;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.html.Option;
import javax.ws.rs.POST;
import java.util.List;
import java.util.Map;
import java.util.Optional;


@Controller
@RequestMapping(path = "/users")
public class UserController {

    private IUserService userService;
    private UserRepository userRepository;

    @Autowired
    public void setUserService(IUserService userService) {
        this.userService = userService;
    }
    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @RequestMapping(value = "/loggedUsers", method = RequestMethod.GET,  produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getLoggedUsers() {
        return new ResponseEntity<>(userService.getUsersFromSessionRegistry(), HttpStatus.OK);
    }

    @PostMapping(value = "/activateUser", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity activateUser(@RequestBody Map<String, String> requestParams){
        userService.activateUser(requestParams.get("code"), requestParams.get("username"));
        return new ResponseEntity<>("Successful activation!", HttpStatus.OK);
    }

    @GetMapping(value = "/user",  produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getUser(@RequestParam(value = "id", required = false)  String id, @RequestParam(value = "email", required = false) String mail)
            throws UserNotFoundException {
        if (id != null) {
            Optional<User> user = userRepository.findById(id);
            if(user.isPresent())
                return new ResponseEntity<>(user.get(), HttpStatus.OK);
            throw new UserNotFoundException(id);
        }
        else if(mail!= null){
            Optional<User> user = userRepository.findOneByEmail(mail);
            if(user.isPresent())
                return new ResponseEntity<>(user.get(), HttpStatus.OK);
            throw new UserNotFoundException(true, mail);
        }
        else
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @GetMapping(value = "/allUsers",  produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<User>> getUsers()
            throws UserNotFoundException {
        return new ResponseEntity<>(userRepository.findAll(), HttpStatus.OK);
    }


    @RequestMapping(value = "/loggedUsersFromSessionRegistry", method = RequestMethod.GET,  produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<String>> getLoggedUsersFromSessionRegistry() {
        return new ResponseEntity<>(Lists.newArrayList(), HttpStatus.OK);
    }

    @PostMapping(value="/changeUsername",  produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> changeUsername(@RequestBody Map<String, String> requestParams){
        if(userService.changeUsername(requestParams.get("id"), requestParams.get("newUsername"), requestParams.get("password")))
            return new ResponseEntity<>("Username changed", HttpStatus.OK);
        return new ResponseEntity<>("Cannot change username", HttpStatus.BAD_REQUEST);
    }

    @PostMapping(value="/changePassword",  produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> changePassword(@RequestBody Map<String, String> requestParams){
        if(userService.changePassword(requestParams.get("id"), requestParams.get("password"), requestParams.get("newPassword")))
            return new ResponseEntity<>("Password changed", HttpStatus.OK);
        return new ResponseEntity<>("Cannot change password", HttpStatus.BAD_REQUEST);
    }

    @PostMapping(value="/changeEmail",  produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> changeEmail(@RequestBody Map<String, String> requestParams){
        if(userService.changeEmail(requestParams.get("id"), requestParams.get("password"), requestParams.get("newEmail")))
            return new ResponseEntity<>("Password changed", HttpStatus.OK);
        return new ResponseEntity<>("Cannot change password", HttpStatus.BAD_REQUEST);
    }

}
