package pl.edu.agh.ki.inz.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import pl.edu.agh.ki.inz.persistence.model.Group;
import pl.edu.agh.ki.inz.persistence.model.User;
import pl.edu.agh.ki.inz.service.interfaces.EmailService;
import pl.edu.agh.ki.inz.util.SetupDataLoader;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collector;

/**
 * Created by Kisiel on 20.11.2017.
 */
@Component
public class EmailServiceImpl implements EmailService {

	private static final Logger logger = LogManager.getLogger(EmailService.class);
	private JavaMailSender emailSender;

	@Autowired
	public void setEmailSender(JavaMailSender emailSender) {
		this.emailSender = emailSender;
	}

	@Override
	@Async
	public void sendSimpleMessage(String to, String subject, String text) {
		logger.info(String.format("Sending mail: %s to: %s", subject, to));
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(to);
		message.setSubject(subject);
		message.setText(text);
		emailSender.send(message);
	}

	@Override
	@Async
	public void sendNotifications(Set<Group> groups, String title, String fullName, LocalDateTime expirationDate) {
		Set<User> users = new HashSet<>();
		groups.stream().map(g -> users.addAll(g.getUsers()));
		String text = "Dear user! \nNew survey: " + title + " for you was created by " + fullName+". \nSurvey expiration date: "+expirationDate +"Best regards, \nSurvey app administration";
		String sub = "New survey created!";
		users.forEach(u->sendSimpleMessage(u.getEmail(), sub, text));
	}


}
