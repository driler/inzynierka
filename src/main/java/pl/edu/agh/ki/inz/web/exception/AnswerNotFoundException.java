package pl.edu.agh.ki.inz.web.exception;

/**
 * Created by Kisiel on 03.12.2017.
 */
public class AnswerNotFoundException extends RuntimeException {
	public AnswerNotFoundException(Long ans) {
		super("Answer with given id not found: " + ans);
	}
}
