package pl.edu.agh.ki.inz.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import pl.edu.agh.ki.inz.persistence.model.Question;
import pl.edu.agh.ki.inz.persistence.model.Survey;

import java.util.List;
import java.util.Optional;

/**
 * Created by Kisiel on 10.06.2017.
 */
public interface QuestionRepository extends CrudRepository<Question, Long> {
	Optional<Question> findById(Long id);

	List<Question> findBySurvey(Survey survey);
}
