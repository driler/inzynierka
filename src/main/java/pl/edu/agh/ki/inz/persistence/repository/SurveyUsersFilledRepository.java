package pl.edu.agh.ki.inz.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.edu.agh.ki.inz.persistence.model.Survey;
import pl.edu.agh.ki.inz.persistence.model.SurveyUsersFilled;
import pl.edu.agh.ki.inz.persistence.model.User;

import java.util.List;

/**
 * Created by Kisiel on 04.12.2017.
 */
public interface SurveyUsersFilledRepository extends JpaRepository<SurveyUsersFilled, Long> {

	List<SurveyUsersFilled> findAllBySurveyAndFilledByOrderByAttemptDesc(Survey survey, User user);
	List<SurveyUsersFilled> findAllByFilledByOrderByFillTimeDesc(User user);
	List<SurveyUsersFilled> findAllBySurveyOrderByFilledBy(Survey survey);
}
