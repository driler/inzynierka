package pl.edu.agh.ki.inz.util.data;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import pl.edu.agh.ki.inz.serializer.desarializer.FilledSurveyDeserializer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kisiel on 30.11.2017.
 */
@JsonDeserialize(using = FilledSurveyDeserializer.class)
public class FilledSurveyPrimaryData {
	private String surveyId;
	private String userId;
	private List<SimpleQuestion> questions = new ArrayList<>();

	public String getSurveyId() {
		return surveyId;
	}

	public void setSurveyId(String surveyId) {
		this.surveyId = surveyId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<SimpleQuestion> getQuestions() {
		return questions;
	}

	public void setQuestions(List<SimpleQuestion> questions) {
		this.questions = questions;
	}

	public void addToList(SimpleQuestion q){
		this.questions.add(q);
	}

	public static class SimpleQuestion{
		private Long question;
		private List<Long> answers = new ArrayList<>();

		public SimpleQuestion() {
		}

		public SimpleQuestion(Long question, List<Long> answers) {
			this.question = question;
			this.answers = answers;
		}

		public Long getQuestion() {
			return question;
		}

		public void setQuestion(Long question) {
			this.question = question;
		}

		public List<Long> getAnswers() {
			return answers;
		}

		public void setAnswers(List<Long> answers) {
			this.answers = answers;
		}

		public void addToList(Long i){
			this.answers.add(i);
		}
	}

}
