package pl.edu.agh.ki.inz.serializer.converter;

import com.fasterxml.jackson.databind.util.StdConverter;
import pl.edu.agh.ki.inz.persistence.model.User;


/**
 * Created by Kisiel on 02.06.2017.
 */
public class UserConverter extends StdConverter<User, String> {
    @Override
    public String convert(User value) {
        return value.getFirstName() + " " + value.getLastName();
    }
}