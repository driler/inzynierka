package pl.edu.agh.ki.inz.persistence.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import pl.edu.agh.ki.inz.serializer.converter.SurveyIdConverter;
import pl.edu.agh.ki.inz.serializer.converter.UserConverter;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by Kisiel on 04.12.2017.
 */
@Entity
public class SurveyUsersFilled {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@ManyToOne
	@JsonSerialize(converter = SurveyIdConverter.class)
	@JoinColumn(name = "survey")
	private Survey survey;
	private LocalDateTime fillTime = LocalDateTime.now();
	@ManyToOne
	@JsonSerialize(converter = UserConverter.class)
	@JoinColumn(name = "filledBy")
	private User filledBy;
	private int attempt =1;

	public SurveyUsersFilled(Survey survey, User user) {
		this.survey = survey;
		this.filledBy = user;
	}

	public SurveyUsersFilled() {
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Survey getSurvey() {
		return survey;
	}

	public void setSurvey(Survey survey) {
		this.survey = survey;
	}

	public LocalDateTime getFillTime() {
		return fillTime;
	}

	public void setFillTime(LocalDateTime fillTime) {
		this.fillTime = fillTime;
	}

	public User getFilledBy() {
		return filledBy;
	}

	public void setFilledBy(User filledBy) {
		this.filledBy = filledBy;
	}

	public int getAttempt() {
		return attempt;
	}

	public void setAttempt(int attempt) {
		this.attempt = attempt;
	}
}
