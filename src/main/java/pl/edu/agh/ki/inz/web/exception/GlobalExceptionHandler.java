package pl.edu.agh.ki.inz.web.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.agh.ki.inz.util.GenericResponse;

/**
 * Created by Kisiel on 10.06.2017.
 */
@ControllerAdvice
@RestController
public class GlobalExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(value = UserNotFoundException.class)
    public ResponseEntity<GenericResponse> handleUserNotFoundException(UserNotFoundException e){
        logger.warn(e.getMessage());
        return new ResponseEntity<>(new GenericResponse(e.getMessage(), "User not found"), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = GroupNotFoundException.class)
    public ResponseEntity<GenericResponse> handleGroupNotFoundException(GroupNotFoundException e){
        logger.warn(e.getMessage());
        return new ResponseEntity<>(new GenericResponse(e.getMessage(), "Group not found"), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = SurveyNotFoundException.class)
    public ResponseEntity<GenericResponse> handleSurveyNotFoundException(SurveyNotFoundException e){
        logger.warn(e.getMessage());
        return new ResponseEntity<>(new GenericResponse(e.getMessage(), "Survey not found"), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = AccessDeniedException.class)
    public ResponseEntity<GenericResponse> handleAccessDenied(AccessDeniedException e){
        logger.warn(e.getMessage());
        return new ResponseEntity<>(new GenericResponse(e.getMessage(), "Access denied"), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<GenericResponse> handleException(Exception e){
        logger.warn(e.getMessage());
        e.printStackTrace();
        return new ResponseEntity<>(new GenericResponse(e.getMessage(), "Internal Exception"), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
