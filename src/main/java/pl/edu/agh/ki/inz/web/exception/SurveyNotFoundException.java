package pl.edu.agh.ki.inz.web.exception;

/**
 * Created by Kisiel on 10.06.2017.
 */
public class SurveyNotFoundException extends RuntimeException {
    public SurveyNotFoundException(long id) {
        super("Survey not find with given id: " + id);
    }

	public SurveyNotFoundException(String id) {
		super("Survey not find with given id: " + id);
	}
}
