package pl.edu.agh.ki.inz.util.data;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import pl.edu.agh.ki.inz.persistence.model.Survey;
import pl.edu.agh.ki.inz.serializer.SurveyPrimaryDataSerializer;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by Kisiel on 16.11.2017.
 */
@JsonSerialize(using = SurveyPrimaryDataSerializer.class)
//in this case, it should be a serializer
public class SurveyPrimaryData {
	private String id;
	private String title;
	private String description;
	private String creator;
	private String expirationDate;

	public SurveyPrimaryData(String id, String title, String description, String creator, LocalDateTime expirationDate) {
		this.id = id;
		this.title = title;
		this.description = description;
		this.creator = creator;
		this.expirationDate = expirationDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
	}

	public SurveyPrimaryData(Survey survey) {
		this(survey.getId(), survey.getTitle(), survey.getDescription(), survey.getCreator().getFullName(), survey.getExpirationDate());
	}

	public String getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getCreator() {
		return creator;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

}
