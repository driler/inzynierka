package pl.edu.agh.ki.inz.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import pl.edu.agh.ki.inz.persistence.model.*;
import pl.edu.agh.ki.inz.persistence.repository.*;
import pl.edu.agh.ki.inz.service.interfaces.EmailService;
import pl.edu.agh.ki.inz.util.data.FilledSurveyPrimaryData;
import pl.edu.agh.ki.inz.util.data.SurveyPrimaryData;
import pl.edu.agh.ki.inz.service.interfaces.ISurveyService;
import pl.edu.agh.ki.inz.task.CloseSurveyTask;
import pl.edu.agh.ki.inz.task.SurveyTask;
import pl.edu.agh.ki.inz.util.data.SurveyStatisticsData;
import pl.edu.agh.ki.inz.util.data.SurveyUserFilledData;
import pl.edu.agh.ki.inz.web.exception.AnswerNotFoundException;
import pl.edu.agh.ki.inz.web.exception.QuestionNotFoundException;
import pl.edu.agh.ki.inz.web.exception.SurveyNotFoundException;
import pl.edu.agh.ki.inz.web.exception.UserNotFoundException;


import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Kisiel on 07.06.2017.
 */

@Service
public class SurveyService implements ISurveyService {
	private static final Logger logger = LoggerFactory.getLogger(SurveyService.class);

	private SurveyRepository surveyRepository;
	private AnswerRepository answerRepository;
	private QuestionRepository questionRepository;
	private UserRepository userRepository;
	private UsersAnswersRepository usersAnswersRepository;
	private SurveyUsersFilledRepository surveyUsersFilledRepository;
	private EmailService emailService;

	@Autowired
	public void setSurveyRepository(SurveyRepository surveyRepository) {
		this.surveyRepository = surveyRepository;
	}

	@Autowired
	public void setAnswerRepository(AnswerRepository answerRepository) {
		this.answerRepository = answerRepository;
	}

	@Autowired
	public void setQuestionRepository(QuestionRepository questionRepository) {
		this.questionRepository = questionRepository;
	}

	@Autowired
	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Autowired
	public void setUsersAnswersRepository(UsersAnswersRepository usersAnswersRepository){
		this.usersAnswersRepository = usersAnswersRepository;
	}

	@Autowired
	public void setSurveyUsersFilledRepository(SurveyUsersFilledRepository surveyUsersFilledRepository) {
		this.surveyUsersFilledRepository = surveyUsersFilledRepository;
	}

	@Autowired
	public void setEmailService(EmailService emailService) {
		this.emailService = emailService;
	}

	@Override
	public void save(Survey survey) {
		logger.info("Saving survey: " + survey.getTitle());
		surveyRepository.save(survey);
	}

	@Override
	public String saveNewSurvey(Survey survey) {
		survey.setCreationDate(LocalDateTime.now());
		survey.setOpen(true);
		logger.info("Saving new survey: " + survey.getTitle());
		survey.getQuestions().forEach(question -> {
			logger.info("Saving question: '" + question.getQuestion() + "' and answers");
			answerRepository.save(question.getAnswers());
			questionRepository.save(question);
		});
		surveyRepository.save(survey);
		survey.getQuestions().forEach(question -> {
			question.setSurvey(survey);
			questionRepository.save(question);
		});

//		emailService.sendNotifications(survey.getGroups(), survey.getTitle(), survey.getCreator().getFullName(), survey.getExpirationDate());

		SurveyTask task = new CloseSurveyTask();
		task.setSurvey(survey);
		task.setSurveyRepository(surveyRepository);
		Timer timer2 = new Timer();
		timer2.schedule(task, Date.from(survey.getExpirationDate().atZone(ZoneId.systemDefault()).toInstant()));
		return survey.getId();
	}

	@Override
	public void addGroupToSurvey(Survey survey, Group group) {
		survey.addGroup(group);
		surveyRepository.save(survey);
	}

	@Override
	public void addQuestionToSurvey(Survey survey, Question question) {
		logger.info("Saving question: '" + question.getQuestion() + "' and answers for survey: " + survey.getTitle());
		answerRepository.save(question.getAnswers());
		questionRepository.save(question);
		survey.addQuestion(question);
		question.setSurvey(survey);
		surveyRepository.save(survey);
		questionRepository.save(question);
	}

	@Override
	public Set<SurveyPrimaryData> findSurveysForUser(String username) {
		Optional<User> userOptional = userRepository.findByUsername(username);
		if (!userOptional.isPresent())
			throw new UserNotFoundException(username);

		Set<Survey> surveySet = new HashSet<>();
		List<Survey> surveys = surveyRepository.findAllByOpen(true);
		surveys.forEach(survey -> {
			if (!survey.getCreator().getUsername().equals(username)){
				survey.getGroups().forEach(group -> {
					if (group.checkIfUserBelongsToGroup(userOptional.get()))
						if (!survey.isOneTime() ||
								surveyUsersFilledRepository.findAllBySurveyAndFilledByOrderByAttemptDesc(survey,userOptional.get()).isEmpty())
							surveySet.add(survey);
				});}
		});

		return surveySet.stream().map(SurveyPrimaryData::new).collect(Collectors.toCollection(HashSet::new));
	}

	@Override
	public Set<SurveyPrimaryData> findSurveysCreatedByUser(String id) {
		Optional<User> userOptional = userRepository.findById(id);
		if (!userOptional.isPresent())
			throw new UserNotFoundException(id);

		List<Survey> surveys = surveyRepository.findAllByCreator(userOptional.get());
		Set<Survey> surveySet = new HashSet<Survey>(surveys);

		return surveySet.stream().map(SurveyPrimaryData::new).collect(Collectors.toCollection(HashSet::new));
	}

	@Override
	//todo check if valid repository method
	public List<Survey> findSurveyForGroup(Group group) {
		return surveyRepository.findAllByGroups(group);
	}

	@Override
	public void fillSurvey(FilledSurveyPrimaryData data) {
		logger.info(String.format("Filling survey: %s by user: %s", data.getSurveyId(), data.getUserId()));
		Optional<Survey> surveyOptional = surveyRepository.findOneById(data.getSurveyId());
		Optional<User> userOptional = userRepository.findById(data.getUserId());

		if(surveyOptional.isPresent() && userOptional.isPresent()) {
			Survey survey = surveyOptional.get();
			SurveyUsersFilled info = new SurveyUsersFilled(survey, userOptional.get());

			List<SurveyUsersFilled> usersAnswersList = surveyUsersFilledRepository
					.findAllBySurveyAndFilledByOrderByAttemptDesc(survey,userOptional.get());

			if (!usersAnswersList.isEmpty())
				info.setAttempt(usersAnswersList.get(0).getAttempt()+1);
			surveyUsersFilledRepository.save(info);

			List<UsersAnswers> list = new ArrayList<>();

			data.getQuestions().forEach(simpleQuestion -> {
				Question question = questionRepository.findById(simpleQuestion.getQuestion())
						.orElseThrow(()-> new QuestionNotFoundException(simpleQuestion.getQuestion()));

				simpleQuestion.getAnswers().forEach(ans ->{
					Answer answer = answerRepository.findById(ans).orElseThrow(()-> new AnswerNotFoundException(ans));
					UsersAnswers usersAnswer = new UsersAnswers();
					usersAnswer.setAnswer(answer);
					usersAnswer.setFillingInfo(info);
					usersAnswer.setQuestion(question);

					list.add(usersAnswer);
				});
			});
			usersAnswersRepository.save(list);

		}
		else {
			if(!surveyOptional.isPresent())
				throw new SurveyNotFoundException(data.getSurveyId());
			else
				throw new UserNotFoundException(data.getUserId());
		}
	}

	@Override
	public List<SurveyUserFilledData> findSurveyHistoryForUser(String username) {
		List<SurveyUserFilledData> list = new ArrayList<>();
		User user = userRepository.findByUsername(username).orElseThrow(()-> new UserNotFoundException(username));
		List<SurveyUsersFilled> surveys = surveyUsersFilledRepository.findAllByFilledByOrderByFillTimeDesc(user);
		surveys.forEach(s-> list.add(new SurveyUserFilledData(s.getSurvey(), s.getId(), s.getFillTime())));

		return list;
	}

	@Override
	public Map<String, Long> findUsersFilledSurvey(Survey survey) {
		return surveyUsersFilledRepository.findAllBySurveyOrderByFilledBy(survey)
				.stream().collect(Collectors.groupingBy(e -> e.getFilledBy().getFullName(), Collectors.counting()));
	}

	public Object prepareStatistics(Survey survey) {
		logger.debug("Generating statistics for survey: " + survey.getTitle());
		return new SurveyStatisticsData(survey, usersAnswersRepository.findAllByQuestionIn(survey.getQuestions()),
				surveyUsersFilledRepository.findAllBySurveyOrderByFilledBy(survey).size());
	}


	public List<FilledSurveyPrimaryData.SimpleQuestion> findUserAnswersForSurvey(String userId, Survey survey, Long filledId) {
		User user = userRepository.findById(userId).orElseThrow(()-> new UserNotFoundException(userId));
		SurveyUsersFilled surveyUsersFilled = surveyUsersFilledRepository.findOne(filledId);
		if(surveyUsersFilled.getFilledBy()!= user || surveyUsersFilled.getSurvey() != survey)
			throw new AccessDeniedException("You are not allowed to read this data!");

		Map<Long, List<Long>> answers = usersAnswersRepository.findAllByFillingInfo(surveyUsersFilled).stream()
				.collect(Collectors.groupingBy(e -> e.getQuestion().getId(),
				Collectors.mapping(e->e.getAnswer().getId(), Collectors.toList())));

		List<FilledSurveyPrimaryData.SimpleQuestion> questionList = new ArrayList<>();
		answers.forEach((key, value) -> questionList.add(new FilledSurveyPrimaryData.SimpleQuestion(key, value)));
		return questionList;
	}
}
