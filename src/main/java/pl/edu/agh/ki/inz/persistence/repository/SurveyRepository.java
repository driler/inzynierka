package pl.edu.agh.ki.inz.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.edu.agh.ki.inz.persistence.model.Group;
import pl.edu.agh.ki.inz.persistence.model.Survey;
import pl.edu.agh.ki.inz.persistence.model.User;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Created by Kisiel on 07.06.2017.
 */
public interface SurveyRepository extends JpaRepository<Survey, Long> {

    List<Survey> findAllByGroups(Group group);
    List<Survey> findAllByGroups(Long groupID);
    List<Survey> findAllByCreator(User user);
    List<Survey> findAllByOpen(boolean open);
    List<Survey> findAllByExpirationDate(LocalDateTime dateTime);
    Optional<Survey> findOneById(String id);
}
