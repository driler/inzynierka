package pl.edu.agh.ki.inz.persistence.model;

import pl.edu.agh.ki.inz.persistence.model.type.AnswerType;

import javax.persistence.*;

/**
 * Created by Kisiel on 05.06.2017.
 */
@Entity
public class Answer {
    private Long id;
    private String content;
    private AnswerType answerType;

    public Answer() {
    }

    public Answer(String content) {
        this.content = content;
        answerType = AnswerType.CLOSE;
    }

    public Answer(String content, AnswerType answerType) {
        this.content = content;
        this.answerType = answerType;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public AnswerType getAnswerType() {
        return answerType;
    }

    public void setAnswerType(AnswerType answerType) {
        this.answerType = answerType;
    }
}
