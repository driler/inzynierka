package pl.edu.agh.ki.inz.web.exception;

/**
 * Created by Kisiel on 03.12.2017.
 */
public class QuestionNotFoundException extends RuntimeException  {
	public QuestionNotFoundException(Long qid) {
		super("Question with given id not found: " + qid);
	}
}
