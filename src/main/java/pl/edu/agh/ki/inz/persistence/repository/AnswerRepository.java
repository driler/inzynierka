package pl.edu.agh.ki.inz.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.edu.agh.ki.inz.persistence.model.Answer;
import pl.edu.agh.ki.inz.persistence.model.Group;

import java.util.Optional;

/**
 * Created by Kisiel on 10.06.2017.
 */
public interface AnswerRepository extends JpaRepository<Answer, Long> {

	Optional<Answer> findById(Long id);
}
