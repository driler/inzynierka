package pl.edu.agh.ki.inz.service;

import org.apache.logging.log4j.LogManager;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.agh.ki.inz.persistence.model.Group;
import pl.edu.agh.ki.inz.persistence.model.User;
import pl.edu.agh.ki.inz.persistence.repository.GroupRepository;
import pl.edu.agh.ki.inz.persistence.repository.SurveyRepository;
import pl.edu.agh.ki.inz.persistence.repository.UserRepository;
import pl.edu.agh.ki.inz.util.data.UserPrimaryData;
import pl.edu.agh.ki.inz.service.interfaces.IGroupService;
import pl.edu.agh.ki.inz.web.exception.GroupNotFoundException;
import pl.edu.agh.ki.inz.web.exception.UserNotFoundException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kisiel on 26.05.2017.
 */
@Service
public class GroupService implements IGroupService {

    private static final Logger logger = LogManager.getLogger(GroupService.class);

    private GroupRepository groupRepository;
    private SurveyRepository surveyRepository;
    private UserRepository userRepository;

    @Autowired
    public void setGroupRepository(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }
    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void save(Group group) {
        groupRepository.save(group);
    }

    @Override
    public void addUsersToGroup(List<User> users, Group group){
        logger.info("Adding " + users.size() + " users to group: " +group.getName());
        group.addUsersToGroup(users);
        groupRepository.save(group);
    }


    @Override
    public void addUserToGroup(User user, Group group) {
        logger.info("Adding user: " +user.getEmail() +" to group: " + group.getName());
        group.addUserToGroup(user);
        logger.info("Saving group: " + group.getName());
        groupRepository.save(group);
    }

    @Override
    public void addUserToGroup(String username, Group group) {
        addUserToGroup((userRepository.findByUsername(username).orElseThrow(() -> new UserNotFoundException(username))), group);
    }

    @Override
    public void addUserToGroup(String username, String groupName) throws GroupNotFoundException, UserNotFoundException{
        addUserToGroup((userRepository.findByUsername(username).orElseThrow(() -> new UserNotFoundException(username))),
                (groupRepository.findByName(groupName).orElseThrow(() -> new GroupNotFoundException(groupName))));
    }

    @Override
    public void addUserToGroup(User user, String groupName) throws GroupNotFoundException {
        addUserToGroup(user,
            (groupRepository.findByName(groupName).orElseThrow(() -> new GroupNotFoundException(groupName))));
    }

    @Override
    public List<Object> returnUsersOfGroup(String groupname){
        List<Object> list = new ArrayList<>();

        groupRepository.findByName(groupname).orElseThrow(() -> new GroupNotFoundException(groupname))
                .getUsers().forEach(user -> list.add(UserPrimaryData.copyImportantData(user)));
        return list;
    }

}
