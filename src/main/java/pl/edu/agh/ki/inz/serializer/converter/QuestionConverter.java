package pl.edu.agh.ki.inz.serializer.converter;

import com.fasterxml.jackson.databind.util.Converter;
import com.fasterxml.jackson.databind.util.StdConverter;
import pl.edu.agh.ki.inz.persistence.model.Question;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Kisiel on 01.12.2017.
 */
public class QuestionConverter extends StdConverter<Question, Map<String, Object>> {

	@Override
	public Map<String, Object> convert(Question question) {

		Map<String, Object> map = new HashMap<>();
		map.put("id", question.getId());
		map.put("question", question.getQuestion());
		map.put("type", question.getType());
		map.put("answers", question.getAnswers());
		return map;
	}
}
