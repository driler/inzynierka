package pl.edu.agh.ki.inz.serializer.converter;

import com.fasterxml.jackson.databind.util.Converter;
import com.fasterxml.jackson.databind.util.StdConverter;
import pl.edu.agh.ki.inz.persistence.model.Survey;

/**
 * Created by Kisiel on 03.12.2017.
 */
public class SurveyIdConverter extends StdConverter<Survey, String> {
	@Override
	public String convert(Survey value) {
		return value.getId();
	}
}
