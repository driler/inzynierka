package pl.edu.agh.ki.inz.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import pl.edu.agh.ki.inz.serializer.converter.GroupSetConverter;
import pl.edu.agh.ki.inz.util.Generator;


import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "app_user")
public class User implements UserDetails {

	@Id
	@GeneratedValue(generator="uuid2")
	@GenericGenerator(name="uuid2", strategy = "uuid2")
	private String id;
	private String firstName;
	private String lastName;
	@Column(unique = true)
	private String username;
	@Column(unique = true)
	private String email;
	@Column(length = 60)
	private String password;
	private boolean enabled;
	private boolean accountExpired = false;
	private boolean accountLocked = false;
	private boolean credentialsExpired = false;
	private boolean accountActivated = false;
	private String invCode = Generator.randomStringGenerator(8);
	private double activationCode = Generator.randomDouble();

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "user_authority",
			joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "authority_id", referencedColumnName = "id"))
	private Set<Authority> authorities;

	@JsonSerialize(converter = GroupSetConverter.class)
	@ManyToMany(mappedBy = "users", fetch = FetchType.EAGER)
	private Set<Group> groups = new HashSet<>();

	public User() {
		super();
		this.enabled = false;
	}

	public User(String firstName, String lastName, String password, String email, String username, boolean enabled) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.username = username;
		this.password = password;
		this.email = email;
		this.enabled = enabled;
	}

	public User(String id, String firstName, String lastName, String username, String email) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.username = username;
		this.email = email;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.authorities;
	}

	public Set<Authority> getAuthoritiesAsList() {
		return this.authorities;
	}

	public void setAuthorities(Set<Authority> authorities) {
		this.authorities = new HashSet<>(authorities);
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(final String password) {
		this.password = password;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(final boolean enabled) {
		this.enabled = enabled;
	}

	public Set<Group> getGroups() {
		return groups;
	}

	public void setGroups(Set<Group> groups) {
		this.groups = groups;
	}

	public String getUsername() {
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		return !accountExpired;
	}

	@Override
	public boolean isAccountNonLocked() {
		return !accountLocked;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return !credentialsExpired;
	}

	public void setAccountExpired(boolean accountExpired) {
		this.accountExpired = accountExpired;
	}

	public void setAccountLocked(boolean accountLocked) {
		this.accountLocked = accountLocked;
	}

	public void setCredentialsExpired(boolean credentialsExpired) {
		this.credentialsExpired = credentialsExpired;
	}

	public boolean isAccountActivated() {
		return accountActivated;
	}

	public void setAccountActivated(boolean accountActivated) {
		this.accountActivated = accountActivated;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getInvCode() {
		return invCode;
	}

	public void setInvCode(String invCode) {
		this.invCode = invCode;
	}

	@JsonIgnore
	public double getActivationCode() {
		return activationCode;
	}

	public void setActivationCode(double activationCode) {
		this.activationCode = activationCode;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((email == null) ? 0 : email.hashCode());
		return result;
	}
////todo ogolnie metoda spoko, ale testy nie przechodza, bo jest sprawdzanie kazdego usera z kazdym, chyba ze przerobic testy
//    @Override
//    public boolean equals(final Object obj) {
//        if (this == obj)
//            return true;
//
//        if (obj == null)
//            return false;
//
//        if (getClass() != obj.getClass())
//            return false;
//
//        final User user = (User) obj;
//        return email.equals(user.email) && username.equals(user.getUsername());
//    }

	@Override
	public String toString() {
		return "User [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName +
				", email=" + email + ", password=" + password + ", enabled=" + enabled +
				", groups=" + groups +
				"]";
	}

	public String basicInfo() {
		return "User [firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + "]";
	}

	public String getFullName() {
		return firstName + " " + lastName;
	}
}