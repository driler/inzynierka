package pl.edu.agh.ki.inz.util.data;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.edu.agh.ki.inz.persistence.model.Answer;
import pl.edu.agh.ki.inz.persistence.model.Question;
import pl.edu.agh.ki.inz.persistence.model.Survey;
import pl.edu.agh.ki.inz.persistence.model.UsersAnswers;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Kisiel on 06.12.2017.
 */
public class SurveyStatisticsData {
	private static final Logger logger = LogManager.getLogger(SurveyStatisticsData.class);

	private String title;
	private String description;
	private LocalDateTime expirationDate;
	private List<QuestionSummary> questionSummaryList = new ArrayList<>();
	private int numberOfFilling;

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public LocalDateTime getExpirationDate() {
		return expirationDate;
	}

	public List<QuestionSummary> getQuestionSummaryList() {
		return questionSummaryList;
	}

	public int getNumberOfFilling() {
		return numberOfFilling;
	}

	public SurveyStatisticsData(Survey survey, List<UsersAnswers> list, int size) {
		this.numberOfFilling = size;
		this.title = survey.getTitle();
		this.description = survey.getDescription();
		this.expirationDate = survey.getExpirationDate();

		Map<Question, List<UsersAnswers>> map = list.stream().collect(Collectors.groupingBy(UsersAnswers::getQuestion));

		map.forEach((key, value) -> {
			logger.debug("generating statistic for question: " + key.getQuestion());
			QuestionSummary summary = new QuestionSummary(survey.isAnonymous());
			summary.setQuestion(key.getQuestion());
			summary.setAnswers(value.stream().collect(Collectors.groupingBy(e -> e.getAnswer().getContent(), Collectors.counting())));
			summary.addNotAnsweredAnswers(key.getAnswers(), survey.isAnonymous());

			if (!survey.isAnonymous())
				summary.setUsersAnswers(value.stream()
						.collect(Collectors.groupingBy(e -> e.getAnswer().getContent(),
								Collectors.mapping(e -> e.getFillingInfo().getFilledBy().getFullName(), Collectors.toList()))));

			questionSummaryList.add(summary);
		});
	}

	public static class QuestionSummary {
		private String question;
		private List<AnswerSummary> answers;
		private List<UsersAnswersSummary> usersAnswers;

		public String getQuestion() {
			return question;
		}

		public void setQuestion(String question) {
			this.question = question;
		}

		public List<AnswerSummary> getAnswers() {
			return answers;
		}

		public void setAnswers(Map<String, Long> answers) {
			answers.forEach((key, value) -> this.answers.add(new AnswerSummary(key, value)));
		}

		public List<UsersAnswersSummary> getUsersAnswers() {
			return usersAnswers;
		}

		void setUsersAnswers(Map<String, List<String>> usersAnswers) {
			usersAnswers.forEach((key, value) -> this.usersAnswers.add(new UsersAnswersSummary(key, value)));
		}

		QuestionSummary(boolean isAnonymous) {
			if (!isAnonymous)
				usersAnswers = new ArrayList<>();
			this.answers = new ArrayList<>();
		}

		public void addNotAnsweredAnswers(Set<Answer> answers, boolean isAnonymous) {
			List<String> list = this.answers.stream().map(AnswerSummary::getContent).collect(Collectors.toList());
			answers.forEach(answer -> {
				if (!list.contains(answer.getContent())) {
					this.answers.add(new AnswerSummary(answer.getContent(), 0L));
					if(!isAnonymous)
						this.usersAnswers.add(new UsersAnswersSummary(answer.getContent(), new ArrayList<>()));
				}
			});
		}

		private static class AnswerSummary {
			private String content;
			private Long numberOfAnswers;

			public String getContent() {
				return content;
			}

			public void setContent(String content) {
				this.content = content;
			}

			public Long getNumberOfAnswers() {
				return numberOfAnswers;
			}

			public void setNumberOfAnswers(Long numberOfAnswers) {
				this.numberOfAnswers = numberOfAnswers;
			}

			AnswerSummary(String content, Long numberOfAnswers) {
				this.content = content;
				this.numberOfAnswers = numberOfAnswers;
			}
		}

		private static class UsersAnswersSummary {
			private String content;
			private List<String> usersAnswers;

			public String getContent() {
				return content;
			}

			public void setContent(String content) {
				this.content = content;
			}

			public List<String> getUsersAnswers() {
				return usersAnswers;
			}

			public void setUsersAnswers(List<String> usersAnswers) {
				this.usersAnswers = usersAnswers;
			}

			UsersAnswersSummary(String content, List<String> usersAnswers) {
				this.content = content;
				this.usersAnswers = usersAnswers;
			}
		}
	}

}
