package pl.edu.agh.ki.inz.web.exception;

public final class UserNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 5861310537366287163L;

    public UserNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }


    public UserNotFoundException(final Throwable cause) {
        super(cause);
    }

    public UserNotFoundException(String usernname) {
        super("User with given id not found: " + usernname);
    }

    public UserNotFoundException(boolean flag , String mai1) {
        super("user with given email not found: " + mai1);
    }
}
