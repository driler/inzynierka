package pl.edu.agh.ki.inz.util.data;

import pl.edu.agh.ki.inz.persistence.model.Group;
import pl.edu.agh.ki.inz.persistence.model.User;

/**
 * Created by Kisiel on 16.11.2017.
 */
//@JsonSerialize(using = UserPrimaryDataSerializer.class)
//maybe it is not necessary to user json serializer with classes like this?
//todo: think about it
public class UserPrimaryData {
	private String id;
	private String firstName;
	private String lastName;
	private String username;
	private String email;
	private boolean accountActivated;
	private String[] groupSet;

	public UserPrimaryData() {
	}

	public UserPrimaryData(String id, String firstName, String lastName, String username, String email, String[] groupSet, boolean accountActivated) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.username = username;
		this.email = email;
		this.groupSet = groupSet;
		this.accountActivated = accountActivated;
	}

	public static UserPrimaryData copyImportantData(User user) {
		return new UserPrimaryData(user.getId(),user.getFirstName(), user.getLastName(), user.getUsername(), user.getEmail(), user.getGroups().stream().map(Group::getName).toArray(String[]::new), user.isAccountActivated());
	}

	public String getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getUsername() {
		return username;
	}

	public String getEmail() {
		return email;
	}

	public String[] getGroupSet() {
		return groupSet;
	}

	public boolean isAccountActivated() {
		return accountActivated;
	}
}
