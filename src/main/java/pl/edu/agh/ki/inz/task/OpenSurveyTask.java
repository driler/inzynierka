package pl.edu.agh.ki.inz.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.TimerTask;

/**
 * Created by Kisiel on 26.11.2017.
 */
public class OpenSurveyTask extends SurveyTask{

	private static final Logger logger = LogManager.getLogger(OpenSurveyTask.class);

	@Override
	public void run() {
		if(surveyRepository != null)
			if (!survey.isOpen()){
				logger.info("Open survey: " + survey.getTitle());
				survey.setOpen(true);
				surveyRepository.save(survey);
			}
			else
				logger.error("Survey repo is null...");
	}

}
