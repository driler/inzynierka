package pl.edu.agh.ki.inz.persistence.repository;


import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import pl.edu.agh.ki.inz.persistence.model.User;

import javax.swing.text.html.Option;
import java.util.Optional;

@Qualifier("userRepository")
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByEmail(String email);

    @Override
    void delete(User user);

    Optional<User> findByFirstName(String name);

    Optional<User> findByUsername(String username);

	Optional<User> findOneByUsernameAndPassword(String username, String password);
	Optional<User> findOneByEmail(String email);

	Optional<User> findById(String id);
}
