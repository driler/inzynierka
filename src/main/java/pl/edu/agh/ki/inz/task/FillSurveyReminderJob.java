package pl.edu.agh.ki.inz.task;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.edu.agh.ki.inz.persistence.repository.SurveyRepository;

import java.time.LocalDateTime;

/**
 * Created by Kisiel on 14.12.2017.
 */
@Component
public class FillSurveyReminderJob implements Job {

	private SurveyRepository surveyRepository;

	@Autowired
	public void setSurveyRepository(SurveyRepository surveyRepository) {
		this.surveyRepository = surveyRepository;
	}

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		//todo
		//surveyRepository.findAllByExpirationDate(LocalDateTime)
	}
}
