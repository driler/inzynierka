package pl.edu.agh.ki.inz.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.edu.agh.ki.inz.persistence.model.Question;
import pl.edu.agh.ki.inz.persistence.model.Survey;
import pl.edu.agh.ki.inz.persistence.repository.QuestionRepository;
import pl.edu.agh.ki.inz.persistence.repository.SurveyRepository;
import pl.edu.agh.ki.inz.service.QuestionService;
import pl.edu.agh.ki.inz.service.SurveyService;
import pl.edu.agh.ki.inz.web.exception.SurveyNotFoundException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Kisiel on 12.06.2017.
 */
@Controller
@RequestMapping(path = "/questions")
public class QuestionController {
    private QuestionRepository questionRepository;
    private SurveyRepository surveyRepository;
    private SurveyService surveyService;
    private QuestionService questionService;

    @Autowired
    public void setQuestionRepository(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }
    @Autowired
    public void setSurveyRepository(SurveyRepository surveyRepository) {
        this.surveyRepository = surveyRepository;
    }

    @Autowired
    public void setSurveyService(SurveyService surveyService) {
        this.surveyService = surveyService;
    }

    @Autowired
    public void setQuestionService(QuestionService questionService) {
        this.questionService = questionService;
    }

    @PostMapping(path = "/addQuestion", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity addQuestionToSurvey(@RequestParam(value = "surveyId") long surveyId, @RequestBody Question question){
        Survey survey = surveyRepository.findOne(surveyId);
        if(survey != null){
            surveyService.addQuestionToSurvey(survey,question);
            Map<String,String> map = new HashMap<>();
            map.put("message", "Question was added!");
            map.put("question_id", String.valueOf(question.getId()));
            return new ResponseEntity<>(map, HttpStatus.OK);
        }
        throw new SurveyNotFoundException(surveyId);
    }

}
