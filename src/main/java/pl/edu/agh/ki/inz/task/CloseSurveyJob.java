package pl.edu.agh.ki.inz.task;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.edu.agh.ki.inz.persistence.model.Survey;
import pl.edu.agh.ki.inz.persistence.repository.SurveyRepository;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Kisiel on 06.11.2017.
 */
@Component
public class CloseSurveyJob implements Job {

	private SurveyRepository surveyRepository;

	@Autowired
	public void setSurveyRepository(SurveyRepository surveyRepository) {
		this.surveyRepository = surveyRepository;
	}

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		List<Survey> surveys = surveyRepository.findAllByOpen(true);
		surveys.forEach(survey -> {
			if(survey.getExpirationDate().isBefore(LocalDateTime.now())){
				survey.setOpen(false);
				surveyRepository.save(survey);
			}
		});
	}
}
