package pl.edu.agh.ki.inz.web.exception;

/**
 * Created by Kisiel on 02.06.2017.
 */
public class GroupNotFoundException extends RuntimeException {

    public GroupNotFoundException(long id) {
        super("Group not find with given id: " + id);
    }

    public GroupNotFoundException(String name) {
        super("Group not find with given group name: " + name);
    }
}
