package pl.edu.agh.ki.inz.service.interfaces;


import pl.edu.agh.ki.inz.persistence.model.Group;
import pl.edu.agh.ki.inz.persistence.model.Survey;
import pl.edu.agh.ki.inz.persistence.model.User;

import java.util.List;
import java.util.Optional;

/**
 * Created by Kisiel on 26.05.2017.
 */
public interface IGroupService {
    void save(Group group);

	void addUsersToGroup(List<User> users, Group group);

	void addUserToGroup(User user, Group group);
    void addUserToGroup(String username, Group group);
    void addUserToGroup(String username, String groupName);
    void addUserToGroup(User user, String groupName);

	List<Object> returnUsersOfGroup(String groupname);
}
