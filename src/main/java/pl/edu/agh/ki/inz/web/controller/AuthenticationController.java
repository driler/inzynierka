package pl.edu.agh.ki.inz.web.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.edu.agh.ki.inz.service.UserService;
import pl.edu.agh.ki.inz.util.UserTokenState;
import pl.edu.agh.ki.inz.security.TokenHelper;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Created by fan.jin on 2017-05-10.
 */

@RestController
@RequestMapping(value = "/auth", produces = APPLICATION_JSON_VALUE)
public class AuthenticationController {

	private TokenHelper tokenHelper;
	private UserService userService;

	@Autowired
	public void setTokenHelper(TokenHelper tokenHelper) {
		this.tokenHelper = tokenHelper;
	}

	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	@Value("${jwt.expires_in}")
	private int EXPIRES_IN;

	@Value("${jwt.cookie}")
	private String TOKEN_COOKIE;

	@RequestMapping(value = "/refresh", method = RequestMethod.GET)
	public ResponseEntity<?> refreshAuthenticationToken(HttpServletRequest request, HttpServletResponse response) {

		String authToken = tokenHelper.getToken(request);
		if (authToken != null && tokenHelper.canTokenBeRefreshed(authToken)) {
			// TODO check user password last update
			String refreshedToken = tokenHelper.refreshToken(authToken);

			Cookie authCookie = new Cookie(TOKEN_COOKIE, (refreshedToken));
			authCookie.setPath("/");
			authCookie.setHttpOnly(true);
			authCookie.setMaxAge(EXPIRES_IN);
			// Add cookie to response
			response.addCookie(authCookie);

			UserTokenState userTokenState = new UserTokenState(refreshedToken, EXPIRES_IN);
			return ResponseEntity.ok(userTokenState);
		} else {
			UserTokenState userTokenState = new UserTokenState();
			return ResponseEntity.accepted().body(userTokenState);
		}
	}

	@PostMapping(value = "/register", produces = APPLICATION_JSON_VALUE)
	public ResponseEntity registerUser(@RequestBody Map<String, String> requestParams) {
		userService.registerNewUserAccount(requestParams);
		return new ResponseEntity<>("User created!", HttpStatus.OK);
	}
}