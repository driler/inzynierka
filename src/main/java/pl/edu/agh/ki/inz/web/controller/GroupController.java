package pl.edu.agh.ki.inz.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.edu.agh.ki.inz.persistence.model.Group;
import pl.edu.agh.ki.inz.persistence.repository.GroupRepository;
import pl.edu.agh.ki.inz.service.interfaces.IGroupService;
import pl.edu.agh.ki.inz.web.exception.GroupNotFoundException;

/**
 * Created by Kisiel on 02.06.2017.
 */
@Controller
@RequestMapping("/groups")
public class GroupController {

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private IGroupService groupService;


    @GetMapping(path = "/group", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getGroup(@RequestParam(value = "id") long id)
            throws GroupNotFoundException {
        Group group = groupRepository.findOne(id);
        if(group != null)
            return new ResponseEntity<>(group, HttpStatus.OK);
        throw new GroupNotFoundException(id);
    }


}
