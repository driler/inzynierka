package pl.edu.agh.ki.inz.serializer.converter;

import com.fasterxml.jackson.databind.util.StdConverter;
import pl.edu.agh.ki.inz.persistence.model.Group;
import pl.edu.agh.ki.inz.persistence.model.User;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by Kisiel on 02.06.2017.
 */
public class UserSetConverter extends StdConverter<Set<User>, Map<String, String>> {

    @Override
    public Map<String, String> convert(Set<User> users) {
        return users.stream().collect(Collectors.toMap(User::getId, user -> user.getFirstName() + " " + user.getLastName()));
    }
}

