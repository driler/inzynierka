package pl.edu.agh.ki.inz.persistence.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import pl.edu.agh.ki.inz.persistence.model.type.QuestionType;

import javax.persistence.*;
import javax.validation.constraints.Max;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Kisiel on 05.06.2017.
 */
@Entity
public class Question {

    private Long id;
    private QuestionType type;
    private String question;
    private Set<Answer> answers = new HashSet<>();
    private Survey survey;

    public Question() {
    }

    public Question(QuestionType type, String question) {
        this.type = type;
        this.question = question;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public QuestionType getType() {
        return type;
    }

    public void setType(QuestionType type) {
        this.type = type;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String content) {
        this.question = content;
    }

    @OneToMany
    @JoinColumn(name = "question_id")
    public Set<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(Set<Answer> answers) {
        this.answers = answers;
    }

    @ManyToOne
    @JoinColumn(name = "survey_id")
    public Survey getSurvey() {
        return survey;
    }

    public void setSurvey(Survey survey) {
        this.survey = survey;
    }

    public void addAnswer(Answer answer){
        this.answers.add(answer);
    }

    public void addAnswers(List<Answer> answerList){
        this.answers.addAll(answerList);
    }
}
