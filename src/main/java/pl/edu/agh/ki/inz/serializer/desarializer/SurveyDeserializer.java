package pl.edu.agh.ki.inz.serializer.desarializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.IntNode;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import pl.edu.agh.ki.inz.persistence.model.*;
import pl.edu.agh.ki.inz.persistence.model.type.QuestionType;
import pl.edu.agh.ki.inz.persistence.repository.GroupRepository;
import pl.edu.agh.ki.inz.persistence.repository.UserRepository;
import pl.edu.agh.ki.inz.web.exception.GroupNotFoundException;
import pl.edu.agh.ki.inz.web.exception.UserNotFoundException;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by Kisiel on 10.06.2017.
 */
public class SurveyDeserializer extends JsonDeserializer<Survey> {

	private Map<String, QuestionType> map = ImmutableMap.of("Single-choice", QuestionType.SINGLE, "Multiple-choice", QuestionType.MULTI, "Open", QuestionType.OPEN);

	private GroupRepository groupRepository;
	private UserRepository userRepository;

	@Autowired
	public void setGroupRepository(GroupRepository groupRepository) {
		this.groupRepository = groupRepository;
	}

	@Autowired
	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public Survey deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {

		JsonNode node = p.getCodec().readTree(p);
		String creator = node.get("creatorId").asText();
		String group = node.get("group").asText();

		Optional<User> user = userRepository.findById(creator);
		Optional<Group> gr = groupRepository.findByName(group);

		Survey survey = new Survey(user.orElseThrow(() -> new UserNotFoundException(creator)),
				gr.orElseThrow(() -> new GroupNotFoundException(group)));
		survey.setDescription(node.get("description").asText());
		survey.setAnonymous(node.get("anonymous").asBoolean());
		survey.setOneTime(node.get("oneTime").asBoolean());
        survey.setTitle(node.get("name").asText());
		survey.setExpirationDate(LocalDateTime.parse(node.get("date").asText().concat(" 23:59:59"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));

		JsonNode questions = node.get("questions");
		questions.elements().forEachRemaining(el -> {
			Question question = new Question();
			question.setQuestion(el.get("name").asText());
			question.setType(map.get(el.get("type").asText()));
			el.get("answers").elements().forEachRemaining(e -> question.addAnswer(new Answer(e.get("answer").asText())));
			survey.addQuestion(question);
		});


		return survey;
	}
}
