package pl.edu.agh.ki.inz.service.interfaces;

import org.springframework.security.core.userdetails.UserDetailsService;
import pl.edu.agh.ki.inz.persistence.model.Survey;
import pl.edu.agh.ki.inz.persistence.model.User;


import java.util.List;
import java.util.Map;

public interface IUserService extends UserDetailsService {

	User registerNewUserAccount(Map<String, String> map);

	void saveRegisteredUser(User user);

	void deleteUser(User user);

	User findUserByEmail(String email);

	User getUserByPasswordResetToken(String token);

	User getUserByID(long id);

	//List<String> getUsersFromSessionRegistry();

	boolean emailExist(String email);

	boolean usernameExist(String username);

	List<String> getUsersFromSessionRegistry();

	User findUserByUsername(String username);

	boolean changeUsername(String id, String newUsername, String password);

	boolean changePassword(String id, String password, String newPassword);

	boolean changeEmail(String id, String password, String email);

	void activateUser(String code, String username);
}
