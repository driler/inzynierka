package pl.edu.agh.ki.inz.util.data;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import pl.edu.agh.ki.inz.persistence.model.Survey;
import pl.edu.agh.ki.inz.serializer.SurveyUserFilledDataSerializer;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by Kisiel on 08.12.2017.
 */
@JsonSerialize(using = SurveyUserFilledDataSerializer.class)
public class SurveyUserFilledData extends SurveyPrimaryData {

	private long surveyFilledId;
	private String fillingTime;

	public SurveyUserFilledData(Survey survey) {
		super(survey);
	}

	public SurveyUserFilledData(Survey survey, Long surveyFilledId, LocalDateTime time){
		this(survey);
		this.fillingTime = time.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
		this.surveyFilledId = surveyFilledId;
	}

	public long getSurveyFilledId() {
		return surveyFilledId;
	}

	public String getFillingTime() {
		return fillingTime;
	}

}
