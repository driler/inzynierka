package pl.edu.agh.ki.inz.persistence.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import pl.edu.agh.ki.inz.serializer.converter.UserConverter;
import pl.edu.agh.ki.inz.serializer.converter.UserSetConverter;


import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Kisiel on 26.05.2017.
 */

@JsonIdentityInfo(
		generator = ObjectIdGenerators.PropertyGenerator.class,
		property = "id")
@Entity
@Table(name = "AppGroup")
public class Group {

	private Long id;
	private User groupLeader;
	@Column(unique = true)
	private String name;
	private Set<User> users;
	private Set<Survey> surveys;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JsonSerialize(converter = UserConverter.class)
	@JoinColumn(name = "group_leader")
	public User getGroupLeader() {
		return groupLeader;
	}

	public void setGroupLeader(User groupLeader) {
		this.groupLeader = groupLeader;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JsonSerialize(converter = UserSetConverter.class)
	@JoinTable(name = "user_groups", joinColumns = @JoinColumn(name = "group_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"))
	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	@ManyToMany(mappedBy = "groups")
	public Set<Survey> getSurveys() {
		return surveys;
	}

	public void setSurveys(Set<Survey> surveys) {
		this.surveys = surveys;
	}

	public Group() {
		users = new HashSet<>();
		surveys = new HashSet<>();
	}

	public Group(User groupLeader, String name) {
		this.groupLeader = groupLeader;
		this.name = name;
	}

	public void addUserToGroup(User user) {
		users.add(user);
	}

	public void addUsersToGroup(List<User> userList){
		this.users.addAll(userList);
	}

	public boolean checkIfUserBelongsToGroup(User user) {
		return users.stream().anyMatch(user::equals);
	}
}
