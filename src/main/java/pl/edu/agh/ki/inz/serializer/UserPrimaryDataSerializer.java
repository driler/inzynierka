package pl.edu.agh.ki.inz.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import pl.edu.agh.ki.inz.util.data.UserPrimaryData;

import java.io.IOException;

/**
 * Created by Kisiel on 16.11.2017.
 */
public class UserPrimaryDataSerializer extends JsonSerializer<UserPrimaryData> {

	@Override
	public void serialize(UserPrimaryData value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
		gen.writeStartObject();
		gen.writeStringField("id", value.getId());
		gen.writeStringField("email", value.getEmail());
		gen.writeStringField("username", value.getUsername());
		gen.writeStringField("firstName", value.getFirstName());
		gen.writeStringField("lastName", value.getLastName());
		gen.writeEndObject();
	}
}
