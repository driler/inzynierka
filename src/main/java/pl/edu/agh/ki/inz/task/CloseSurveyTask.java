package pl.edu.agh.ki.inz.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by Kisiel on 26.11.2017.
 */
public class CloseSurveyTask extends SurveyTask{

	private static final Logger logger = LogManager.getLogger(CloseSurveyTask.class);

	@Override
	public void run() {
		if(surveyRepository != null)
			if (survey.isOpen()){
				logger.info("Closing survey: " + survey.getTitle());
				survey.setOpen(true);
				surveyRepository.save(survey);
			}
			else
				logger.error("Survey repo is null...");
	}

}