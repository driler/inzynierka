package pl.edu.agh.ki.inz.serializer.converter;

import com.fasterxml.jackson.databind.util.Converter;
import com.fasterxml.jackson.databind.util.StdConverter;
import pl.edu.agh.ki.inz.persistence.model.Answer;
import pl.edu.agh.ki.inz.persistence.model.Question;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Kisiel on 10.06.2017.
 */
public class QuestionSetConverter extends StdConverter<Set<Question>, List<Object>> {
    @Override
    public List<Object> convert(Set<Question> questions) {
        List<Object> list = new ArrayList<>();

        questions.forEach(question -> {
            Map<String, Object> map = new HashMap<>();
            map.put("id", question.getId());
            map.put("question", question.getQuestion());
            map.put("type", question.getType());
            map.put("answers", question.getAnswers());
            list.add(map);
        });

        return list;
    }
}
