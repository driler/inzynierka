package pl.edu.agh.ki.inz.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.edu.agh.ki.inz.persistence.model.Survey;
import pl.edu.agh.ki.inz.persistence.model.User;
import pl.edu.agh.ki.inz.persistence.repository.SurveyRepository;
import pl.edu.agh.ki.inz.util.data.FilledSurveyPrimaryData;
import pl.edu.agh.ki.inz.util.data.SurveyPrimaryData;
import pl.edu.agh.ki.inz.service.SurveyService;
import pl.edu.agh.ki.inz.web.exception.SurveyNotFoundException;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Created by Kisiel on 10.06.2017.
 */
@Controller
@RequestMapping("/surveys")
//@PreAuthorize("hasAuthority('ROLE_CREATE_SURVEY')")
public class SurveyController {

	private SurveyRepository surveyRepository;
	private SurveyService surveyService;

	@Autowired
	public void setSurveyRepository(SurveyRepository surveyRepository) {
		this.surveyRepository = surveyRepository;
	}

	@Autowired
	public void setSurveyService(SurveyService surveyService) {
		this.surveyService = surveyService;
	}

	@GetMapping(path = "/survey", produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<Survey> getSurvey(@RequestParam(value = "id") String id) {
		Optional<Survey> survey = surveyRepository.findOneById(id);
		return new ResponseEntity<>(survey.orElseThrow(() -> new SurveyNotFoundException(id)), HttpStatus.OK);
	}

	@PostMapping(path = "/addSurvey", produces = APPLICATION_JSON_VALUE)
	public ResponseEntity createNewSurvey(@RequestBody Survey survey) {
		String id = surveyService.saveNewSurvey(survey);
		Map<String, String> map = new HashMap<>();
		map.put("message", "Survey was created!");
		map.put("survey_id", id);
		return new ResponseEntity<>(map, HttpStatus.OK);
	}

	@GetMapping(path = "/getSurveysForUser", produces = APPLICATION_JSON_VALUE)
	public ResponseEntity getSurveysForUser(@RequestParam(value = "username") String username) {
		Set<SurveyPrimaryData> surveys = surveyService.findSurveysForUser(username);
		return new ResponseEntity<>(surveys, HttpStatus.OK);
	}

	@GetMapping(path = "/getSurveysCreatedByUser", produces = APPLICATION_JSON_VALUE)
	public ResponseEntity getSurveysCreatedByUser(@RequestParam(value = "id") String id) {
		Set<SurveyPrimaryData> surveys = surveyService.findSurveysCreatedByUser(id);
		return new ResponseEntity<>(surveys, HttpStatus.OK);
	}

	@GetMapping(path = "/getSurveyHistoryForUser", produces = APPLICATION_JSON_VALUE)
	public ResponseEntity getSurveyHistoryForUser(@RequestParam(value = "username") String username) {
		return new ResponseEntity<>(surveyService.findSurveyHistoryForUser(username), HttpStatus.OK);
	}

	@PostMapping(path = "/fillSurvey", produces = APPLICATION_JSON_VALUE)
	public ResponseEntity fillSurvey(@RequestBody FilledSurveyPrimaryData data) {
		surveyService.fillSurvey(data);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@GetMapping(path = "getStatistics", produces = APPLICATION_JSON_VALUE)
	public ResponseEntity fillSurvey(@RequestParam(value = "surveyId") String surveyId, @RequestParam(value = "userId") String userid) {
		Survey survey = surveyRepository.findOneById(surveyId).orElseThrow(() -> new SurveyNotFoundException(surveyId));
		if (!survey.getCreator().getId().equals(userid))
			return new ResponseEntity<>("Only survey creator can see this page", HttpStatus.FORBIDDEN);

		return new ResponseEntity<>(surveyService.prepareStatistics(survey), HttpStatus.OK);
	}

	@GetMapping(path = "getUsersFilledSurvey", produces = APPLICATION_JSON_VALUE)
	public ResponseEntity getUsersFilledSurvey(@RequestParam(value = "surveyId") String surveyId) {
		Survey survey = surveyRepository.findOneById(surveyId).orElseThrow(() -> new SurveyNotFoundException(surveyId));
		return new ResponseEntity<>(surveyService.findUsersFilledSurvey(survey), HttpStatus.OK);
	}

	@GetMapping(path = "getUserAnswersForSurvey", produces = APPLICATION_JSON_VALUE)
	public ResponseEntity getUserAnswersForSurvey(@RequestParam String surveyId, @RequestParam String userId, @RequestParam Long filledId){
		Survey survey = surveyRepository.findOneById(surveyId).orElseThrow(() -> new SurveyNotFoundException(surveyId));
		return new ResponseEntity<>(surveyService.findUserAnswersForSurvey(userId, survey, filledId), HttpStatus.OK);
	}
}
