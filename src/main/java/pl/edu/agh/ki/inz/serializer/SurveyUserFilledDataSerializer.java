package pl.edu.agh.ki.inz.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import pl.edu.agh.ki.inz.util.data.SurveyUserFilledData;

import java.io.IOException;

/**
 * Created by Kisiel on 08.12.2017.
 */
public class SurveyUserFilledDataSerializer extends JsonSerializer<SurveyUserFilledData> {
	@Override
	public void serialize(SurveyUserFilledData value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
		gen.writeStartObject();
		gen.writeStringField("id", value.getId());
		gen.writeNumberField("filledId", value.getSurveyFilledId());
		gen.writeStringField("title", value.getTitle());
		gen.writeStringField("description", value.getDescription());
		gen.writeStringField("creator", value.getCreator());
		gen.writeStringField("expirationDate", value.getExpirationDate());
		gen.writeStringField("fillingTime", value.getFillingTime());
		gen.writeEndObject();
	}
}

