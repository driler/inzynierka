package pl.edu.agh.ki.inz;


import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.context.request.RequestContextListener;
import pl.edu.agh.ki.inz.task.CloseSurveyJob;

@Configuration
@EnableAutoConfiguration
@EnableConfigurationProperties
@SpringBootApplication
@EnableAsync
public class Application extends SpringBootServletInitializer {


    private static final Logger logger = LoggerFactory.getLogger("Application");
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);

//        try {
//            prepareAndRunJobs();
//            prepareTimerTask();
//        } catch (Exception e) {
//            logger.error(e.getMessage());
//        }

    }

    private static void prepareAndRunJobs() throws SchedulerException {
        JobDetail job1 = JobBuilder.newJob(CloseSurveyJob.class).withIdentity("closeSurvey", "survey").build();
        Trigger trigger1 = TriggerBuilder.newTrigger().withIdentity("closeSurveyTrigger", "group1")
                .withSchedule(CronScheduleBuilder.cronSchedule("0/5 * * * * ?")).build();
        Scheduler scheduler1 = null;
        try {
            scheduler1 = new StdSchedulerFactory().getScheduler();
            scheduler1.startDelayed(3600);
            scheduler1.scheduleJob(job1, trigger1);
        } catch (SchedulerException e) {
            logger.warn(e.getMessage());
            if(scheduler1 != null && !scheduler1.isShutdown()){
                scheduler1.shutdown();
            }
        }
    }


    @Bean
    public RequestContextListener requestContextListener() {
        return new RequestContextListener();
    }
}

/*todo
 * dodanie kiedy uzupelnil ankiete
 */