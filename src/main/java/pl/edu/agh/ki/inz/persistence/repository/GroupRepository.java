package pl.edu.agh.ki.inz.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.edu.agh.ki.inz.persistence.model.Group;
import pl.edu.agh.ki.inz.persistence.model.User;

import java.util.Optional;
import java.util.Set;

/**
 * Created by Kisiel on 26.05.2017.
 */
public interface GroupRepository extends JpaRepository<Group, Long> {
    Set<Group> findAllByGroupLeader(User user);
    Optional<Group> findByName(String groupName);
}
