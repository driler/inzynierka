package pl.edu.agh.ki.inz.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.edu.agh.ki.inz.persistence.model.Authority;

import java.util.Optional;

/**
 * Created by Kisiel on 04.11.2017.
 */
public interface AuthorityRepository extends JpaRepository<Authority, Long> {
	Optional<Authority> findByName(String privilege);
}
