package pl.edu.agh.ki.inz.persistence.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.GenericGenerator;
import pl.edu.agh.ki.inz.serializer.converter.GroupSetConverter;
import pl.edu.agh.ki.inz.serializer.converter.QuestionSetConverter;
import pl.edu.agh.ki.inz.serializer.converter.UserConverter;
import pl.edu.agh.ki.inz.serializer.desarializer.SurveyDeserializer;


import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Kisiel on 05.06.2017.
 */
@Entity
@JsonDeserialize(using = SurveyDeserializer.class)
public class Survey {

	@Id
	@GeneratedValue(generator="uuid2")
	@GenericGenerator(name="uuid2", strategy = "uuid2")
	private String id;
	private String title;
	private String description;
	private LocalDateTime creationDate = LocalDateTime.now();
	private LocalDateTime expirationDate = creationDate.plusDays(7);
	private boolean oneTime = true;
	private boolean anonymous = false;
	private boolean publicSurvey = false;
	private boolean open = true;

	@ManyToOne
	@JsonSerialize(converter = UserConverter.class)
	@JoinColumn(name = "creator")
	private User creator;

	@JsonSerialize(converter = GroupSetConverter.class)
	@ManyToMany
	@JoinTable(name = "survey_groups", joinColumns = @JoinColumn(name = "survey_id", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "group_id", referencedColumnName = "id"))
	private Set<Group> groups = new HashSet<>();

	@JsonSerialize(converter = QuestionSetConverter.class)
	@OneToMany(mappedBy = "survey", cascade = CascadeType.ALL)
	private Set<Question> questions = new HashSet<>();

//	@ManyToMany(fetch = FetchType.EAGER)
////	@JsonSerialize(converter = UserSetConverter.class)
//	@JsonIgnore
//	@JoinTable(name = "filled_surveys", joinColumns = @JoinColumn(name = "survey_id", referencedColumnName = "id"),
//			inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"))
//	private List<User> userFilled = new ArrayList<>();


	public Survey(User creator) {
		this.creator = creator;
	}

	public Survey() {
	}

	public Survey(User user, Group gr) {
		this.creator = user;
		this.groups.add(gr);
	}

	public Survey(String title, String description, LocalDateTime expirationDate, boolean anonymous, Group group, User creator) {
		this.title = title;
		this.description = description;
		this.expirationDate = expirationDate;
		this.anonymous = anonymous;
		this.groups.add(group);
		this.creator = creator;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Set<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(Set<Question> questions) {
		this.questions = questions;
	}

	public User getCreator() {
		return creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}

	public Set<Group> getGroups() {
		return groups;
	}

	public void setGroups(Set<Group> groups) {
		this.groups = groups;
	}

//	public List<User> getUserFilled() {
//		return userFilled;
//	}
//
//	public void setUserFilled(List<User> userFilled) {
//		this.userFilled = userFilled;
//	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public LocalDateTime getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(LocalDateTime expirationDate) {
		this.expirationDate = expirationDate;
	}

	public void addGroup(Group group) {
		this.groups.add(group);
	}

	public void addQuestion(Question question) {
		this.questions.add(question);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isOneTime() {
		return oneTime;
	}

	public void setOneTime(boolean oneTime) {
		this.oneTime = oneTime;
	}

	public boolean isAnonymous() {
		return anonymous;
	}

	public void setAnonymous(boolean anonymous) {
		this.anonymous = anonymous;
	}

	public boolean isPublicSurvey() {
		return publicSurvey;
	}

	public void setPublicSurvey(boolean publicSurvey) {
		this.publicSurvey = publicSurvey;
	}

	@Override
	public String toString() {
		return "Survey{" +
				"id=" + id +
				", title='" + title + '\'' +
				", desc='" + description + '\'' +
				", creator=" + creator.getFirstName() + " " + creator.getLastName() +
				", creationDate=" + creationDate +
				", expirationDate=" + expirationDate +
				'}';
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	public boolean isOpen() {
		return open;
	}

//	public boolean addFilledUser(User user2) {
//		if(!isOneTime() || !getUserFilled().contains(user2)) {
//			this.userFilled.add(user2);
//			return true;
//		}
//		return false;
//	}
}
