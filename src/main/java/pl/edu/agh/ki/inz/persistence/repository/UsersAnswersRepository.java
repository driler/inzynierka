package pl.edu.agh.ki.inz.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.edu.agh.ki.inz.persistence.model.*;

import java.util.List;
import java.util.Set;

/**
 * Created by Kisiel on 03.12.2017.
 */
public interface UsersAnswersRepository extends JpaRepository<UsersAnswers, Long> {

	List<UsersAnswers> findAllByFillingInfo(SurveyUsersFilled info);
	List<UsersAnswers> findAllByQuestionIn(Set<Question> questions);
}
