package pl.edu.agh.ki.inz.serializer.converter;

import com.fasterxml.jackson.databind.util.StdConverter;
import pl.edu.agh.ki.inz.persistence.model.Group;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by Kisiel on 28.05.2017.
 */
public class GroupSetConverter extends StdConverter<Set<Group>, Map<Long, String>> {

    @Override
    public Map<Long, String> convert(Set<Group> groups) {
        return groups.stream().collect(Collectors.toMap(Group::getId, Group::getName));
    }
}
