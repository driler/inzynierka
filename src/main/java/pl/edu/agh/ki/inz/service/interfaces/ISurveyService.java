package pl.edu.agh.ki.inz.service.interfaces;

import pl.edu.agh.ki.inz.persistence.model.Group;
import pl.edu.agh.ki.inz.persistence.model.Question;
import pl.edu.agh.ki.inz.persistence.model.Survey;
import pl.edu.agh.ki.inz.util.data.FilledSurveyPrimaryData;
import pl.edu.agh.ki.inz.util.data.SurveyPrimaryData;
import pl.edu.agh.ki.inz.util.data.SurveyUserFilledData;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Kisiel on 07.06.2017.
 */
public interface ISurveyService {

    void save(Survey survey);
    String saveNewSurvey(Survey survey);
    void addGroupToSurvey(Survey survey, Group group);
    void addQuestionToSurvey(Survey survey, Question question);
	Set<SurveyPrimaryData> findSurveysForUser(String username);
	Set<SurveyPrimaryData> findSurveysCreatedByUser(String username);

	//todo check if valid repository method
	List<Survey> findSurveyForGroup(Group group);

	void fillSurvey(FilledSurveyPrimaryData data);

	List<SurveyUserFilledData> findSurveyHistoryForUser(String username);

	Map<String, Long> findUsersFilledSurvey(Survey survey);
}
