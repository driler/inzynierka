package pl.edu.agh.ki.inz.util;


import pl.edu.agh.ki.inz.util.data.UserPrimaryData;

public class UserTokenState {

    private UserPrimaryData userData;
    private String token;
    private Long expires_in;

    public UserTokenState() {
        this.token = null;
        this.expires_in = null;
    }

    public UserTokenState(String token, long expires_in) {
        this.token = token;
        this.expires_in = expires_in;
    }

    public UserTokenState(UserPrimaryData userData, String token, Long expires_in) {
        this.userData = userData;
        this.token = token;
        this.expires_in = expires_in;
    }

    public UserPrimaryData getUserData() {
        return userData;
    }

    public void setUserData(UserPrimaryData userData) {
        this.userData = userData;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(Long expires_in) {
        this.expires_in = expires_in;
    }

}