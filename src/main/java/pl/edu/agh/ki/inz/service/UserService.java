package pl.edu.agh.ki.inz.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import pl.edu.agh.ki.inz.persistence.model.*;

import pl.edu.agh.ki.inz.persistence.repository.AuthorityRepository;
import pl.edu.agh.ki.inz.persistence.repository.GroupRepository;
import pl.edu.agh.ki.inz.persistence.repository.PasswordResetTokenRepository;
import pl.edu.agh.ki.inz.persistence.repository.UserRepository;

import pl.edu.agh.ki.inz.service.interfaces.EmailService;
import pl.edu.agh.ki.inz.service.interfaces.IUserService;
import pl.edu.agh.ki.inz.web.exception.UserAlreadyExistException;
import pl.edu.agh.ki.inz.web.exception.UserNotFoundException;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService implements IUserService {

    private UserRepository userRepository;
    private PasswordResetTokenRepository passwordTokenRepository;
    private SurveyService surveyService;
    private GroupService groupService;
    private SessionRegistry sessionRegistry;
    private EmailService emailService;
    private PasswordEncoder passwordEncoder;
    private GroupRepository groupRepository;
    private AuthorityRepository authorityRepository;


    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setPasswordTokenRepository(PasswordResetTokenRepository passwordTokenRepository) {
        this.passwordTokenRepository = passwordTokenRepository;
    }
    @Autowired
    public void setSurveyService(SurveyService surveyService) {
        this.surveyService = surveyService;
    }

    @Autowired
    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    @Autowired
    public void setSessionRegistry(SessionRegistry sessionRegistry) {
        this.sessionRegistry = sessionRegistry;
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder){
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }

    @Autowired
    public void setGroupRepository(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    @Autowired
    public void setAuthorityRepository(AuthorityRepository authorityRepository) {
        this.authorityRepository = authorityRepository;
    }

    public UserService() {
    }

    @Override
    public User registerNewUserAccount(Map<String, String> map) {
        if (emailExist(map.get("email")))
            throw new UserAlreadyExistException("There is an account with that email adress: " + map.get("email"));
        final Set<Authority> userPrivileges = new HashSet<>(Collections.singletonList(authorityRepository.findByName("ROLE_READ").get()));
        User user = new User();
        user.setFirstName(map.get("firstName"));
        user.setLastName(map.get("lastName"));
        user.setUsername(map.get("username"));
        user.setPassword(passwordEncoder.encode(map.get("passwordOne")));
        user.setEmail(map.get("email"));
        user.setEnabled(true);
        userRepository.save(user);
        groupService.addUserToGroup(user, groupRepository.findByName("USERS").get());
        user.setAuthorities(userPrivileges);
        userRepository.save(user);

        String text = String.format("Dear %s!\nThank you for register in survey app!\nBelow you can find code, which you have to enter in user panel in app to use all functionalities.\n\nYour code: %d \n\nBest regards! \nSurvey app administration", user.getFullName(), (long)user.getActivationCode() );
        emailService.sendSimpleMessage(user.getEmail(), "Code for accout activation", text);

        return user;
    }



    @Override
    public void saveRegisteredUser(final User user) {
        userRepository.save(user);
    }

    @Override
    public void deleteUser(final User user) {
        final PasswordResetToken passwordToken = passwordTokenRepository.findByUser(user);

        if (passwordToken != null)
            passwordTokenRepository.delete(passwordToken);

        userRepository.delete(user);
    }


    @Override
    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email).orElseThrow(() -> new UserNotFoundException(email));
    }

    @Override
    public User getUserByPasswordResetToken(final String token) {
        return passwordTokenRepository.findByToken(token).getUser();
    }

    @Override
    public User getUserByID(final long id) {
        return userRepository.findOne(id);
    }

    @Override
    public boolean emailExist(final String email) {
        return userRepository.findByEmail(email).isPresent();
    }

    @Override
    public boolean usernameExist(final String username) {
        return userRepository.findByUsername(username).isPresent();
    }

    @Override
    public List<String> getUsersFromSessionRegistry() {
        return sessionRegistry.getAllPrincipals().stream().filter((u) -> !sessionRegistry.getAllSessions(u, false)
                .isEmpty()).map(Object::toString).collect(Collectors.toList());
    }

    @Override
    public User findUserByUsername(String username) {
        return userRepository.findByFirstName(username).orElseThrow(() -> new UserNotFoundException("User not found: "+ username));
    }

    @Override
    public boolean changeUsername(String id, String newUsername, String password) {
        User user = userRepository.findById(id).orElseThrow(()-> new UserNotFoundException(id));
        if(passwordEncoder.matches(password, user.getPassword())){
            user.setUsername(newUsername);
            userRepository.save(user);
            return true;
        }
        return false;
    }

    @Override
    public boolean changePassword(String id, String password, String newPassword) {
        System.out.println(id);
        System.out.println(userRepository.findById(id).isPresent());
        User user = userRepository.findById(id).orElseThrow(()-> new UserNotFoundException(id));
        if(passwordEncoder.matches(password, user.getPassword())){
            user.setUsername(passwordEncoder.encode(newPassword));
            userRepository.save(user);
            return true;
        }
        return false;
    }

    @Override
    public boolean changeEmail(String id, String password, String email) {
        User user = userRepository.findById(id).orElseThrow(()-> new UserNotFoundException(id));
        if(passwordEncoder.matches(password, user.getPassword())){
            user.setEmail(email);
            userRepository.save(user);
            System.out.println(user.toString());
            emailService.sendSimpleMessage(user.getEmail(), "Email changed", "Email has been changed");
            return true;
        }
        return false;
    }

    @Override
    public void activateUser(String code, String username) {
        User user = userRepository.findByUsername(username).orElseThrow(()-> new UserNotFoundException(username));
        if(user.getActivationCode()!=Double.valueOf(code))
            throw new RuntimeException("Cannot activate user - wrong activation code!");
        user.setAccountActivated(true);
        userRepository.save(user);
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return this.userRepository.findByUsername(username).orElseThrow(() -> new UserNotFoundException(username));
    }

}