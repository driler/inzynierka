package pl.edu.agh.ki.inz.service.interfaces;

import pl.edu.agh.ki.inz.persistence.model.Group;

import java.time.LocalDateTime;
import java.util.Set;

/**
 * Created by Kisiel on 20.11.2017.
 */
public interface EmailService {
	void sendSimpleMessage(
			String to, String subject, String text);

	void sendNotifications(Set<Group> groups, String title, String fullName, LocalDateTime expirationDate);
}
