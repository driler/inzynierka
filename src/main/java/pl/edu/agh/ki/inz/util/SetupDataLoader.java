package pl.edu.agh.ki.inz.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.agh.ki.inz.persistence.model.*;
import pl.edu.agh.ki.inz.persistence.model.type.AnswerType;
import pl.edu.agh.ki.inz.persistence.model.type.QuestionType;
import pl.edu.agh.ki.inz.persistence.repository.*;
import pl.edu.agh.ki.inz.service.GroupService;
import pl.edu.agh.ki.inz.service.SurveyService;
import pl.edu.agh.ki.inz.task.OpenSurveyTask;
import pl.edu.agh.ki.inz.task.SurveyTask;
import pl.edu.agh.ki.inz.util.data.FilledSurveyPrimaryData;

import java.time.LocalDateTime;
import java.util.*;

@Component
public class SetupDataLoader implements ApplicationListener<ContextRefreshedEvent> {
	private static final Logger logger = LogManager.getLogger(SetupDataLoader.class);

	@Value("${bootstrap.data}")
	private boolean bootstrapData;


	private UserRepository userRepository;
	private PasswordEncoder passwordEncoder;
	private GroupRepository groupRepository;
	private GroupService groupService;
	private AuthorityRepository authorityRepository;
	private SurveyRepository surveyRepository;
	private QuestionRepository questionRepository;
	private AnswerRepository answerRepository;
	private SurveyUsersFilledRepository surveyUsersFilledRepository;

	private SurveyService surveyService;

	@Autowired
	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Autowired
	public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}

	@Autowired
	public void setGroupRepository(GroupRepository groupRepository) {
		this.groupRepository = groupRepository;
	}

	@Autowired
	public void setGroupService(GroupService groupService) {
		this.groupService = groupService;
	}

	@Autowired
	public void setSurveyRepository(SurveyRepository surveyRepository) {
		this.surveyRepository = surveyRepository;
	}

	@Autowired
	public void setQuestionRepository(QuestionRepository questionRepository) {
		this.questionRepository = questionRepository;
	}

	@Autowired
	public void setAnswerRepository(AnswerRepository answerRepository) {
		this.answerRepository = answerRepository;
	}

	@Autowired
	public void setAuthorityRepository(AuthorityRepository authorityRepository) {
		this.authorityRepository = authorityRepository;
	}

	@Autowired
	public void setSurveyUsersFilledRepository(SurveyUsersFilledRepository surveyUsersFilledRepository) {
		this.surveyUsersFilledRepository = surveyUsersFilledRepository;
	}

	@Autowired
	public void setSurveyService(SurveyService surveyService) {
		this.surveyService = surveyService;
	}

	@Override
	@Transactional
	public void onApplicationEvent(final ContextRefreshedEvent event) {

		final Authority readAuthority = createAuthorityIfNotFound("ROLE_READ");
		final Authority writeAuthority = createAuthorityIfNotFound("ROLE_WRITE");
		final Authority passwordAuthority = createAuthorityIfNotFound("ROLE_CHANGE_PASSWORD");
		final Authority createSurveyAuthority = createAuthorityIfNotFound("ROLE_CREATE_SURVEY");

		final Set<Authority> adminPrivileges = new HashSet<>(Arrays.asList(readAuthority, writeAuthority, passwordAuthority, createSurveyAuthority));
		final Set<Authority> userPrivileges = new HashSet<>(Arrays.asList(readAuthority, passwordAuthority));
		final Set<Authority> superPrivileges = new HashSet<>(Arrays.asList(readAuthority, passwordAuthority, createSurveyAuthority));

		User user = createUserIfNotFound("Test", "Test2", "admin", "test@test.com", "admin");
		User user2 = createUserIfNotFound("AAAAAAA", "AAAA", "aaa", "aaaa@aaaa.com", "aa");
		User user3 = createUserIfNotFound("Jan", "Kowalski", "kowal", "jankowalski@test.com", "jan.kowalski");
//		User user4 = createUserIfNotFound("Kisiel", "Kisiel", "kisiel", "kisielewski.adam@gmail.com", "kisiel");


		userRepository.save(user);
		userRepository.save(user2);
		userRepository.save(user3);

		if (!bootstrapData)
			return;

		logger.info("Bootstraping database");

		final int size = 100;

		//create users
		List<User> userList = createUsers(size);

		//create groups
		List<Group> groups = new ArrayList<>();
		Group admins = createGroupIfNotFound("ADMINS", user);
		Group users = createGroupIfNotFound("USERS", user);
		groups.add(createGroupIfNotFound("Group_1", userList.get(size / 40)));
		groups.add(createGroupIfNotFound("Group_2", userList.get(size / 5)));
		groups.add(createGroupIfNotFound("Group_3", userList.get(size / 10)));
		groups.add(createGroupIfNotFound("Group_4", userList.get(size / 20)));
		groups.add(createGroupIfNotFound("Group_5", userList.get(size / 30)));
		groups.add(createGroupIfNotFound("Group_6", userList.get(size / 25)));

		groupService.addUserToGroup(user2, users);
//		groupService.addUserToGroup(user4, admins);
		groupService.addUserToGroup(user3, users);

		userList.forEach(u -> {
			addAuthoritiesToUser(u, userPrivileges);
		});
		groupService.addUsersToGroup(userList, users);

		addAuthoritiesToUser(user, adminPrivileges);
		addAuthoritiesToUser(user2, userPrivileges);
		addAuthoritiesToUser(user3, superPrivileges);

		prepareSurveys(userList, 300, groups);
	}

	private List<User> createUsers(int number) {
		List<User> users = new ArrayList<>();
		List<String> names = Arrays.asList("Elaina", "Odessa", "Raul", "Henriette", "Mora", "Deandrea", "Zonia", "Dante", "Yer", "Corey", "Katelyn", "Angelo", "Adelaida", "Philomena", "Hyman", "Stan", "Breanne", "Florrie", "Shayne", "Cathie", "Concepcion", "Larry", "Monica", "Branden", "Hollis", "Daine", "Lashaun", "Fred", "Coletta", "Anita", "Darius", "Marquita", "Ariane", "Karon", "Jennell", "Olimpia", "Tequila", "Doretha", "Georgeanna", "Margarett", "Jeannie", "Janett", "Tammera", "Carina", "Lashandra", "Lieselotte", "Carlo", "Victorina", "Annabell", "Elisha", "Marine", "Basil", "Genoveva", "Randolph", "Season", "Maple", "Danyell", "Lissette", "Yoshiko", "Douglas", "Mona", "Alfred", "Erik", "Shanti", "Loreta", "Oscar", "Kandis", "Cortney", "Eusebio", "Shana", "Tynisha", "Jamison", "Lean", "Melinda", "Miyoko", "Antonia", "Janeen", "Shelley", "Star", "Ok", "Piper", "Alene", "Dovie", "Corazon", "Rocco", "Marcella", "Marta", "Julianne", "Loraine", "Elease", "Clair", "Princess", "Golda", "Trang", "Kayleigh", "Delmar", "Senaida", "Reynalda", "Jared", "Selma");
		Random random = new Random();

		for (int i = 0; i < number; i++) {
			String name = names.get(random.nextInt(names.size()));
			String lastname = names.get(random.nextInt(names.size()));
			User user = createUserIfNotFound(name, lastname, "123", name + lastname + "@" + lastname + "." + name + ".com", name + lastname);
			if (user != null)
				users.add(user);
		}

		return users;
	}

	private void prepareSurveys(List<User> userList, int loops, List<Group> groups) {
		Random random = new Random();

		List<Answer> answerList1 = new ArrayList<>();
		for (int i=0; i< loops; i++)
			answerList1.add(new Answer(Generator.randomStringGenerator(random.nextInt(15)), AnswerType.CLOSE));

		answerRepository.save(answerList1);

		List<Question> questionList = new ArrayList<>();

		questionList.add(new Question(QuestionType.SINGLE, "This is some question 0"));
		questionList.add(new Question(QuestionType.SINGLE, "This is some question 1"));
		questionList.add(new Question(QuestionType.SINGLE, "This is some question 2"));
		questionList.add(new Question(QuestionType.MULTI, "This is some question 3"));
		questionList.add(new Question(QuestionType.MULTI, "This is some question 4"));
		questionList.add(new Question(QuestionType.SINGLE, "This is some question 5"));
		questionList.add(new Question(QuestionType.SINGLE, "This is some question 6"));
		questionList.add(new Question(QuestionType.SINGLE, "This is some question 7"));
		questionList.add(new Question(QuestionType.SINGLE, "This is some question 8"));
		questionList.add(new Question(QuestionType.SINGLE, "This is some question 9"));
		questionList.add(new Question(QuestionType.SINGLE, "This is some 10"));
		questionList.add(new Question(QuestionType.MULTI, "This is some 11"));
		questionList.add(new Question(QuestionType.SINGLE, "This is some 12"));
		questionList.add(new Question(QuestionType.MULTI, "This is some 13"));
		questionList.add(new Question(QuestionType.SINGLE, "This is some 14"));
		questionList.add(new Question(QuestionType.SINGLE, "This is some 15"));
		questionList.add(new Question(QuestionType.MULTI, "This is some 16"));
		questionList.add(new Question(QuestionType.SINGLE, "This is some 17"));
		questionList.add(new Question(QuestionType.SINGLE, "This is some 18"));
		questionList.add(new Question(QuestionType.MULTI, "This is some 19"));
		questionList.add(new Question(QuestionType.MULTI, "This is 0"));
		questionList.add(new Question(QuestionType.SINGLE, "This is 1"));
		questionList.add(new Question(QuestionType.SINGLE, "This is 2"));
		questionList.add(new Question(QuestionType.SINGLE, "This is 3"));
		questionList.add(new Question(QuestionType.SINGLE, "This is 4"));
		questionList.add(new Question(QuestionType.MULTI, "This is 5"));
		questionList.add(new Question(QuestionType.SINGLE, "This is 6"));
		questionList.add(new Question(QuestionType.SINGLE, "This is 7"));
		questionList.add(new Question(QuestionType.MULTI, "This is 8"));
		questionList.add(new Question(QuestionType.SINGLE, "This is 9"));
		questionList.add(new Question(QuestionType.SINGLE, "Some question 30?"));
		questionList.add(new Question(QuestionType.SINGLE, "Some question 31?"));
		questionList.add(new Question(QuestionType.SINGLE, "Some question 32?"));
		questionList.add(new Question(QuestionType.MULTI, "Some question 33?"));
		questionList.add(new Question(QuestionType.SINGLE, "Some question 34?"));
		questionList.add(new Question(QuestionType.MULTI, "Some question 35?"));
		questionList.add(new Question(QuestionType.SINGLE, "Some question 36?"));
		questionList.add(new Question(QuestionType.MULTI, "Some question 37?"));
		questionList.add(new Question(QuestionType.SINGLE, "Some question 38?"));
		questionList.add(new Question(QuestionType.SINGLE, "Some question 39?"));

		answerList1.forEach(answer -> questionList.get(random.nextInt(questionList.size())).addAnswer(answer));

		questionRepository.save(questionList);

		List<Survey> surveys = new ArrayList<>();
		surveys.add(new Survey("title", "desc", LocalDateTime.now().plusDays(1), true, groupRepository.findByName("Group_1").get(), userList.get(random.nextInt(userList.size()))));
		surveys.add(new Survey("Other title", "desc", LocalDateTime.now().plusDays(2), true, groupRepository.findByName("Group_2").get(), userList.get(random.nextInt(userList.size()))));
		surveys.add(new Survey("Other survey title", "desc", LocalDateTime.now().plusDays(3), true, groupRepository.findByName("Group_2").get(), userList.get(random.nextInt(userList.size()))));
		surveys.add(new Survey("Other survey", "desc", LocalDateTime.now().plusDays(2), true, groupRepository.findByName("Group_2").get(), userList.get(random.nextInt(userList.size()))));
		surveys.add(new Survey("Some other title", "desc", LocalDateTime.now().plusDays(6), true, groupRepository.findByName("Group_3").get(), userList.get(random.nextInt(userList.size()))));
		surveys.add(new Survey("Some other survey title", "desc", LocalDateTime.now().plusDays(7), true, groupRepository.findByName("Group_2").get(), userList.get(random.nextInt(userList.size()))));
		surveys.add(new Survey("Some other survey", "desc", LocalDateTime.now().plusDays(14), true, groupRepository.findByName("Group_3").get(), userList.get(random.nextInt(userList.size()))));
		surveys.add(new Survey("Some title", "desc", LocalDateTime.now().plusDays(30), true, groupRepository.findByName("Group_6").get(), userList.get(random.nextInt(userList.size()))));

		surveys.forEach(s->{
			int g = random.nextInt(groups.size()/2);
			for (int i=0; i< g; i++)
				s.addGroup(groups.get(random.nextInt(groups.size())));
			System.out.println(s.getCreator().getUsername());
		});
		surveyRepository.save(surveys);

		questionList.forEach(q-> q.setSurvey(surveys.get(random.nextInt(surveys.size()))));
		surveyRepository.save(surveys);
		questionRepository.save(questionList);

		userList.forEach(user -> {
			int s = random.nextInt(surveys.size()/2);
			for (int i=0; i< s; i++) {
				FilledSurveyPrimaryData data = new FilledSurveyPrimaryData();

				Survey survey = surveys.get(random.nextInt(surveys.size()));
				data.setUserId(user.getId());
				data.setSurveyId(survey.getId());

				List<FilledSurveyPrimaryData.SimpleQuestion> questiontmpList = new ArrayList<>();

				questionRepository.findBySurvey(survey).forEach(q->{
					FilledSurveyPrimaryData.SimpleQuestion simpleQuestion = new FilledSurveyPrimaryData.SimpleQuestion();
					simpleQuestion.setQuestion(q.getId());

					simpleQuestion.setAnswers(Collections.singletonList(new ArrayList<>(q.getAnswers()).get(random.nextInt(q.getAnswers().size())).getId()));
					questiontmpList.add(simpleQuestion);

				});
				data.setQuestions(questiontmpList);
				surveyService.fillSurvey(data);
			}
		});

	}

	private void addAuthoritiesToUser(User user, Set<Authority> authorities) {
		logger.info("Adding authorities to user: " + user.getFullName());
		user.setAuthorities(authorities);

	}

	private Authority createAuthorityIfNotFound(String privilege) {
		logger.info("Creating authority: " + privilege);
		Optional<Authority> authorityOptional = authorityRepository.findByName(privilege);
		if (!authorityOptional.isPresent()) {
			Authority authority = new Authority(privilege);
			authorityRepository.save(authority);
			return authority;
		}
		return authorityOptional.get();
	}


	@Transactional
	private Group createGroupIfNotFound(final String name, User leader) {
		logger.info("Creating group: " + name + " with leader: " + leader.basicInfo());
		Optional<Group> groupOpt = groupRepository.findByName(name);
		if (!groupOpt.isPresent()) {
			Group group = new Group();
			group.setGroupLeader(leader);
			group.setName(name);
			group.setUsers(new HashSet<>(Collections.singletonList(leader)));
			groupRepository.save(group);
			return group;
		}
		return groupOpt.get();
	}

	@Transactional
	private User createUserIfNotFound(final String firstName, final String lastName, final String password, final String email, String username) {
		logger.info("Creating user: " + firstName + " " + lastName + " " + email);
		Optional<User> userOptional = userRepository.findByEmail(email);
		if (!userOptional.isPresent()) {
			User user = new User(firstName, lastName, passwordEncoder.encode(password), email, username, true);
			user.setAccountActivated(true);
			userRepository.save(user);
			return user;
		}
		return null;
	}

}